class AppDesignValues {

  static const double defaultPadding = 20;
  static const double bigTopPadding = 70;
  static const double normalTopPadding = 30;
  static const double mediumTopPadding = 50;

  static const double appBarTitleFontSize = 20;
  static const double bigTitleFontSize = 36;
  static const double actionTextFontSize = 17;
}