import 'dart:ui';
import 'package:flutter/material.dart';

class AppColors {

  static const Color appThemeColor = Color.fromRGBO(245, 245, 245, 1);
  static const Color appSecondaryColor = Colors.black;
  static const Color appTextFieldHintColor = Color.fromRGBO(211, 211, 211, 1);
  static const Color appTextFieldBorderColor = Color.fromRGBO(235, 233, 242, 1);
  static const Color appIconColor = Color.fromRGBO(186, 202, 234, 1);
  static const Color appSmallLabelColor = Color.fromRGBO(150, 150, 152, 1);
  static const Color appTextFieldBGColor = Color.fromRGBO(245, 245, 245, 1);
  static const Color appTextPriceBlue = Color.fromRGBO(11, 40, 107, 1);
  static const Color appDeleteButtonColor = Color.fromRGBO(204, 51, 65, 1);
  static const Color appDeleteButtonTextColor = Color.fromRGBO(128, 0, 11, 1);

  static const MaterialColor appThemeMaterialColor = MaterialColor(
    0xFFf5f5f5,
    <int, Color>{
      50: Color(0xFFf5f5f5),
      100: Color(0xFFf5f5f5),
      200: Color(0xFFf5f5f5),
      300: Color(0xFFf5f5f5),
      400: Color(0xFFf5f5f5),
      500: Color(0xFFf5f5f5),
      600: Color(0xFFf5f5f5),
      700: Color(0xFFf5f5f5),
      800: Color(0xFFf5f5f5),
      900: Color(0xFFf5f5f5),
    },
  );
}