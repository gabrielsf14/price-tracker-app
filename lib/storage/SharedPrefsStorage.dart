import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefsStorage {

  static SharedPreferences preferences;

  static init() async {
    preferences = await SharedPreferences.getInstance();
  }

  static read(String key) {
    var data = preferences.getString(key);
    if (data == null)
      return null;
    return json.decode(data);
  }

  static save(String key, value) {
    preferences.setString(key, json.encode(value));
  }

  static remove(String key) {
    preferences.remove(key);
  }
}