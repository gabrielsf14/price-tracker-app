import 'package:pricetracker/validators/validator.dart';

class EmailValidator implements Validator {

  final Pattern pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  final String email;

  EmailValidator(this.email);

  @override
  String errorMessage = "O email digitado não é um email válido.";

  @override
  bool validate() {
    return RegExp(pattern).hasMatch(email);
  }
}