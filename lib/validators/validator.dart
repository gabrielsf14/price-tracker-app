abstract class Validator {

  String errorMessage;

  bool validate();
}