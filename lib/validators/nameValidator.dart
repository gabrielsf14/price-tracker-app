import 'package:pricetracker/validators/validator.dart';

class NameValidator implements Validator {

  final String name;

  NameValidator(this.name);

  @override
  String errorMessage = "O nome não pode ficar em branco.";

  @override
  bool validate() {
    return name.isNotEmpty;
  }
}