import 'package:pricetracker/validators/validator.dart';

class PasswordValidator implements Validator {

  final String password;

  PasswordValidator(this.password);

  @override
  String errorMessage = "A senha deve ter pelo menos 6 caracteres.";

  @override
  bool validate() {
    return password.length > 5;
  }
}