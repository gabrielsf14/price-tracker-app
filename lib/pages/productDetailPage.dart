import 'package:flutter/material.dart';
import 'package:pricetracker/components/borderedButton.dart';
import 'package:pricetracker/components/borderedTextFieldWithIcon.dart';
import 'package:pricetracker/components/circularNetworkImage.dart';
import 'package:pricetracker/components/createLabelDialog.dart';
import 'package:pricetracker/components/defaultAlertDialog.dart';
import 'package:pricetracker/components/defaultAppBar.dart';
import 'package:pricetracker/components/loadingDialog.dart';
import 'package:pricetracker/components/rectangularNetworkImage.dart';
import 'package:pricetracker/design_configs/appColors.dart';
import 'package:pricetracker/design_configs/appDesignValues.dart';
import 'package:pricetracker/models/charData.dart';
import 'package:pricetracker/models/label.dart';
import 'package:pricetracker/models/priceChange.dart';
import 'package:pricetracker/models/product.dart';
import 'package:pricetracker/pages/proPage.dart';
import 'package:pricetracker/repositories/UserRepository.dart';
import 'package:pricetracker/utils/hexColor.dart';
import 'package:pricetracker/validators/emailValidator.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:pricetracker/webservices/createLabelService.dart';
import 'package:pricetracker/webservices/deleteProductService.dart';
import 'package:pricetracker/webservices/getLabelsService.dart';
import 'package:pricetracker/webservices/getProductPriceChangesService.dart';
import 'package:pricetracker/webservices/updateProductLabelService.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:charts_common/common.dart' as common show DateTimeFactory;

class ProductDetailPage extends StatefulWidget {

  Product product;

  ProductDetailPage({Key key, Product product}) : super(key: key) {
    this.product = product;
  }

  @override
  _ProductDetailPageState createState() {
    return product == null ? _ProductDetailPageState() : _ProductDetailPageState.withProduct(product);
  }
}

class _ProductDetailPageState extends State<ProductDetailPage> {

  Product product;

  var labelsPage = 1;
  var allLabels = new List<Label>();
  var seriesList = List<charts.Series>();
  var labelsAndChangesLoaded = false;

  var userIsPro = false;

  _ProductDetailPageState();

  _ProductDetailPageState.withProduct(this.product);

  List<charts.Series<ChartData, DateTime>> createSeriesList(List<ChartData> data) {
    return [
      new charts.Series<ChartData, DateTime>(
        id: 'PriceChanges',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (ChartData data, _) => data.time,
        measureFn: (ChartData data, _) => data.value,
        data: data,
      )
    ];
  }

  void getLabels() {
    GetLabelsService().getLabels(labelsPage).then((labels) {
      setState(() {
        allLabels.addAll(labels);
        if (labels.isNotEmpty) {
          labelsPage++;
          getLabels();
        }
      });
    });
  }

  void getPriceChanges() {
    GetProductPriceChangesService().getPriceChanges(product.id).then((priceChanges) {
      var data = new List<ChartData>();
      setState(() {
        var index = 0;
        priceChanges.forEach((priceChange) {
          if (index == 0) {
            data.add(ChartData(product.getCreatedAtAsDate(), double.parse(product.originalPrice)));
            data.add(ChartData(priceChange.getUpdatedAtAsDate(), double.parse(priceChange.currentPrice)));
          } else {
            var previousPriceChange = priceChanges[index - 1];
            data.add(ChartData(previousPriceChange.getUpdatedAtAsDate(), double.parse(previousPriceChange.currentPrice)));
            data.add(ChartData(priceChange.getUpdatedAtAsDate(), double.parse(priceChange.currentPrice)));
          }
          index++;
        });
        seriesList = createSeriesList(data);
      });
    });
  }

  void updateProductLabel(Label label) {
    LoadingDialog.show(context);
    UpdateProductLabelService().updateProductLabel(product.id, label.name, label.color).then((_) {
      Navigator.pop(context);
      DefaultAlertDialog("Sucesso", "Produto atualizado com sucesso!").show(context);
      setState(() {
        product.label = label;
      });
    }).catchError((error) {
      Navigator.pop(context);
      DefaultAlertDialog("Erro", error.toString()).show(context);
    });
  }

  void createLabel(String name, Color color) {
    LoadingDialog.show(context);
    CreateLabelService().createLabel(HexColor.toHex(color), name).then((label) {
      Navigator.pop(context);
      setState(() {
        allLabels.add(label);
        Navigator.pop(context);
        updateProductLabel(label);
      });
    }).catchError((error) {
      Navigator.pop(context);
      DefaultAlertDialog("Erro", error.toString()).show(context);
    });
  }
  
  @override
  void initState() {
    userIsPro = UserRepository.instance.user.isPro();
    seriesList = createSeriesList([ChartData(DateTime.now(), 0.0)]);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    if (product == null) {
      final Product product = ModalRoute.of(context).settings.arguments;
      this.product = product;
    }

    if (!labelsAndChangesLoaded) {
      labelsAndChangesLoaded = true;
      getLabels();
      getPriceChanges();
    }

    return Scaffold(
      appBar: AppBar (
        title: Text(product.title),
      ),
      body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(AppDesignValues.defaultPadding),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row (
                  children: <Widget>[
                    CircularNetworkImage(20, 20, product.site.image),
                    Padding(
                      padding: EdgeInsets.only(left: 5),
                      child: Text(product.site.name,
                        style: TextStyle(
                            fontSize: 12
                        ),
                      ),
                    ),
                  ],
                ),
                Center (
                  child: RectangularNetworkImage(100, 100, product.image),
                ),
                Padding(padding: EdgeInsets.only(top: 20)),
                Text(
                  product.title,
                  style: TextStyle (
                    fontWeight: FontWeight.bold,
                    fontSize: 14
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 20)),
                Text(
                  product.description,
                  style: TextStyle (
                      fontSize: 14
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 30)),
                Container (
                  margin: EdgeInsets.only(left: 30, right: 30),
                  padding: EdgeInsets.all(10),
                  color: Colors.white,
                  child: Row (
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Column (
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Padding (padding: EdgeInsets.only(top: 1)),
                          Text("Preço atual",
                            style: TextStyle (
                                fontWeight: FontWeight.bold,
                                fontSize: 14
                            ),
                          ),
                          Padding (padding: EdgeInsets.only(top: 12)),
                          Text("Preço original",
                            style: TextStyle (
                              color: Colors.grey,
                              fontSize: 14,
                            ),
                          ),
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(right: 15)),
                      Column (
                        children: <Widget>[
                          Text(
                              product.formattedCurrentPrice(),
                              style: TextStyle (
                                  color: AppColors.appTextPriceBlue,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16
                              )
                          ),
                          Padding(padding: EdgeInsets.only(right: 15, top: 9)),
                          Text(
                              product.formattedPrice(),
                              style: TextStyle (
                                  decoration: TextDecoration.lineThrough,
                                  color: Colors.grey,
                                  fontSize: 16
                              )
                          )
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(left: 15)),
                      Column (
                        children: <Widget>[
                          Icon (
                            product.priceChangeIsPositive() ? Icons.trending_up : product.priceChangeIsZero() ? Icons.trending_flat : Icons.trending_down,
                            size: 18,
                            color: product.priceChangeIsPositive() ? Colors.red : product.priceChangeIsZero() ? Colors.amber : Colors.green,
                          ),
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(left: 8)),
                      Column (
                        children: <Widget>[
                          Padding(padding: EdgeInsets.only(top: 1)),
                          Text(product.getPriceChangeFormatted(),
                            style: TextStyle (
                                color: product.priceChangeIsPositive() ? Colors.red : product.priceChangeIsZero() ? Colors.amber : Colors.green,
                                fontWeight: FontWeight.bold,
                                fontSize: 14
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                Row (
                  children: <Widget>[
                    Expanded (
                      child: BorderedButton("Comprar", () async {
                        if (await canLaunch(product.url))
                          await launch(product.url);
                        else
                          DefaultAlertDialog("Erro", "Não foi possível abrir a link.").show(context);
                      }),
                    )
                  ],
                ),
                Padding(padding: EdgeInsets.only(top:50)),
                Visibility(
                    visible: !userIsPro,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(
                              left: AppDesignValues.defaultPadding,
                              right: AppDesignValues.defaultPadding),
                          child: Text(
                            "Vire PRO para organizar seus produtos em coleções e visualizar o histórico de preços dos produtos de forma gráfica!",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 16, color: AppColors.appSecondaryColor),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 0, right: 0),
                          child: Row(
                            children: [
                              Expanded(
                                child: BorderedButton(
                                    "Virar PRO",
                                        () => {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(builder: (context) => ProPage()),
                                      )
                                    }),
                              )
                            ],
                          ),
                        )
                      ],
                    )
                ),
                Visibility(
                  visible: userIsPro,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Minhas coleções",
                        style: TextStyle (
                            fontWeight: FontWeight.bold,
                            fontSize: 14
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 5)),
                      Container (
                        height: 35,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: allLabels.length + 1,
                          itemBuilder: (context, index) {
                            return InkWell(
                              child: index < allLabels.length ? LabelItem(label: allLabels[index], product: product) : Icon(Icons.add_circle_outline, size: 30),
                              onTap: () {
                                if (index < allLabels.length)
                                {
                                  updateProductLabel(allLabels[index]);
                                } else {
                                  var createLabelDialog = CreateLabelDialog();
                                  createLabelDialog.show(context, (name, color) {
                                    createLabel(name, color);
                                  });
                                }
                              },
                            );
                          },
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 50)),
                      Text(
                        "Histórico de preços",
                        style: TextStyle (
                            fontWeight: FontWeight.bold,
                            fontSize: 14
                        ),
                      ),
                      Container (
                        height: 250,
                        child: charts.TimeSeriesChart(
                            seriesList,
                            animate: true,
                            domainAxis: new charts.EndPointsTimeAxisSpec(
                                tickFormatterSpec: new charts.AutoDateTimeTickFormatterSpec(
                                    day: new charts.TimeFormatterSpec(format: 'dd/MM', transitionFormat: 'dd/MM')
                                )
                            )
                        ),
                      ),
                    ],
                  ),
                ),

                Padding(padding: EdgeInsets.only(top: 30)),
                FlatButton.icon(
                  icon: Icon(Icons.delete, color: AppColors.appDeleteButtonTextColor),
                  label: Text(
                    "Excluir produto",
                    style: TextStyle (
                      color: AppColors.appDeleteButtonTextColor
                    ),
                  ),
                  color: AppColors.appDeleteButtonColor,
                  onPressed: () {
                    LoadingDialog.show(context);
                    DeleteProductService().deleteProduct(product).then((_) {
                      Navigator.pop(context);
                      var alert = DefaultAlertDialog("Sucesso", "Seu produto foi deletado com sucesso!");
                      alert.onClosed = () {
                        Navigator.pop(context, product.id);
                      };
                      alert.show(context);
                    }).catchError((error) {
                      Navigator.pop(context);
                      DefaultAlertDialog("Erro", error.toString()).show(context);
                    });
                  },
                ),
              ],
            ),
          )
      ),
    );
  }
}

class LabelItem extends StatelessWidget {
  final Label label;
  final Product product;

  LabelItem({Key key, this.label, this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container (
      decoration: BoxDecoration (
        borderRadius: new BorderRadius.circular(4),
        color: label.getColorFromHex(),
      ),
      padding: EdgeInsets.only(left: 10, right: 10),
      margin: EdgeInsets.only(right: 10),
      child: Stack (
        children: <Widget>[
          Center (
            child: Text(
              label.name,
              style: TextStyle (
                  fontWeight: label.id == product.label.id ? FontWeight.bold : FontWeight.normal,
                  color: label.id == product.label.id ? Colors.white : Colors.white54
              ),
            ),
          ),
        ],
      )
    );
  }
}
