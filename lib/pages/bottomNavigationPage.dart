import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_inapp_purchase/flutter_inapp_purchase.dart';
import 'package:flutter_inapp_purchase/modules.dart';
import 'package:pricetracker/app_configs/appConfigs.dart';
import 'package:pricetracker/components/circularNetworkImage.dart';
import 'package:pricetracker/components/createLabelDialog.dart';
import 'package:pricetracker/components/defaultAlertDialog.dart';
import 'package:pricetracker/components/addProductAlertDialog.dart';
import 'package:pricetracker/components/loadingDialog.dart';
import 'package:pricetracker/components/optionsAlertDialog.dart';
import 'package:pricetracker/components/rectangularNetworkImage.dart';
import 'package:pricetracker/components/searchTextField.dart';
import 'package:pricetracker/design_configs/appColors.dart';
import 'package:pricetracker/models/user.dart';
import 'package:pricetracker/pages/createAccountPage.dart';
import 'package:pricetracker/pages/loginPage.dart';
import 'package:pricetracker/pages/proPage.dart';
import 'package:pricetracker/pages/productDetailPage.dart';
import 'package:pricetracker/pages/recoverEmailPage.dart';
import 'package:pricetracker/pages/settingsPage.dart';
import 'package:pricetracker/pages/startPage.dart';
import 'package:pricetracker/repositories/UserRepository.dart';
import 'package:pricetracker/utils/hexColor.dart';
import 'package:pricetracker/webservices/createLabelService.dart';
import 'package:pricetracker/webservices/createProductService.dart';
import 'package:pricetracker/webservices/findProductService.dart';
import 'package:pricetracker/webservices/loginService.dart';
import 'package:receive_sharing_intent/receive_sharing_intent.dart';

import 'collectionsPage.dart';
import 'homePage.dart';
import 'lomaddeProductsPage.dart';
import 'offersPage.dart';

class BottomNavigationPage extends StatefulWidget {
  final String title;

  BottomNavigationPage({Key key, this.title}) : super(key: key);

  @override
  _BottomNavigationPageState createState() => _BottomNavigationPageState();
}

class _BottomNavigationPageState extends State<BottomNavigationPage> {

  StreamSubscription intentDataStreamSubscription;

  var pages = [
    HomePage(), OffersPage(), CollectionsPage()
  ];
  var selectedBottomBarIndex = 0;

  Widget appBarTitle = new Text(AppConfigs.appName);
  Icon actionIcon = new Icon(Icons.search);

  Widget appBarTitle2 = new Text("Ofertas");
  Icon actionIcon2 = new Icon(Icons.search);

  var loginDone = false;
  var userIsPro = false;

  @override
  void initState() {
    super.initState();
    userIsPro = UserRepository.instance.user.isPro();
    setupReceiveShareIntent();
    login();
  }

  @override
  void dispose() {
    if (intentDataStreamSubscription != null) {
      intentDataStreamSubscription.cancel();
      intentDataStreamSubscription = null;
    }
    super.dispose();
  }

  void login() {
    var user = UserRepository.instance.user;
    LoginService().login(user.email, user.password).then((_) {
      setState(() {
        loginDone = true;
      });
    }).catchError((_){
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => LoginPage()),
            (Route<dynamic> route) => false,
      );
    });
  }

  void onBottomBarItemTapped(int index) {
    setState(() {
      selectedBottomBarIndex = index;
    });
  }

  void createProduct(String url) {
    LoadingDialog.show(context);
    CreateProductService().createProduct(url).then((product) {
      Navigator.pop(context);
      Navigator.pop(context);
      DefaultAlertDialog("Sucesso", "Seu produto foi cadastrado com sucesso!").show(context);
      setState(() {
        selectedBottomBarIndex = 0;
        var homePage = pages.first as HomePage;
        homePage.reloadProductsList();
      });
    }).catchError((error) {
      Navigator.pop(context);
      DefaultAlertDialog("Erro", error).show(context);
    });
  }

  void setupReceiveShareIntent() {
    intentDataStreamSubscription =
        ReceiveSharingIntent.getTextStream().listen((String value) {
          showAddProductDialogFromIntent(value);
        });

    ReceiveSharingIntent.getInitialText().then((String value) {
      if (value != null) {
        showAddProductDialogFromIntent(value);
      }
    });
  }

  void showAddProductDialogFromIntent(String url) {
    var addProductDialog =  AddProductAlertDialog();
    addProductDialog.textFieldController.text = url;
    addProductDialog.show(context, (url) {
      createProduct(url);
    });
  }

  void createLabel(String name, Color color) {
    LoadingDialog.show(context);
    CreateLabelService().createLabel(HexColor.toHex(color), name).then((label) {
      Navigator.pop(context);
      Navigator.pop(context);
      var collectionsPage = pages[2] as CollectionsPage;
      collectionsPage.reloadLabelsList();
    }).catchError((error) {
      Navigator.pop(context);
      DefaultAlertDialog("Erro", error.toString()).show(context);
    });
  }

  Widget getAppBarForTabSelected() {
    switch (selectedBottomBarIndex) {
      case 0:
        return AppBar(
          title: appBarTitle,
          actions: <Widget>[
            IconButton(icon: actionIcon, onPressed: () {
              setState(() {
                var homePage = pages.first as HomePage;
                if (this.actionIcon.icon == Icons.search) {
                  this.actionIcon = Icon(Icons.close, size: 28);
                  this.appBarTitle = SearchTextField((query) {
                    homePage.state.currentQuery = query;
                    homePage.state.filter();
                  });
                } else {
                  this.actionIcon = Icon(Icons.search, size: 28);
                  this.appBarTitle = Text(AppConfigs.appName);
                  homePage.state.currentQuery = "";
                  homePage.state.filter();
                }
              });
            }),
            IconButton(icon: Icon(Icons.add, size: 28), onPressed: () {
              AddProductAlertDialog().show(context, (url) {
                createProduct(url);
              });
            }),
          ],
        );
        break;
      case 1:
        return AppBar (
          title: appBarTitle2,
          actions: [
            IconButton(icon: actionIcon2, onPressed: () {
              setState(() {
                if (this.actionIcon2.icon == Icons.search) {
                  this.actionIcon2 = Icon(Icons.close, size: 28);
                  this.appBarTitle2 = SearchTextField((query) {}, onSubmitted: (query) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => LomaddeProductsPage(title: '', query: query)),
                    );
                  });
                } else {
                  this.actionIcon2 = Icon(Icons.search, size: 28);
                  this.appBarTitle2 = Text("Ofertas");
                }
              });
            }),
          ],
        );
        break;
      case 2:
        return AppBar (
          title: Text("Minhas coleções"),
          actions: userIsPro ? [
            IconButton(icon: Icon(Icons.add, size: 28), onPressed: () {
              var createLabelDialog = CreateLabelDialog();
              createLabelDialog.show(context, (name, color) {
                createLabel(name, color);
              });
            }),
          ] : [],
        );
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Visibility(
          visible: !loginDone,
          child: Scaffold (
            body: Center(
              child: CircularProgressIndicator(backgroundColor: AppColors.appSecondaryColor),
            ),
          )
        ),
        Visibility (
          visible: loginDone,
          child: Scaffold(
            appBar: getAppBarForTabSelected(),
            body: IndexedStack(
              index: selectedBottomBarIndex,
              children: pages,
            ),
            drawer: Container (
              width: 220,
              child: Drawer(
                child: ListView(
                  padding: EdgeInsets.zero,
                  children: <Widget>[
                    Padding (
                      padding: EdgeInsets.only(top: 50),
                      child: Column (
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
//                          Center (
//                              child: CircularNetworkImage(70, 70, '')
//                          ),
                          Center (
                            child: Text(
                              '\nOlá, ${UserRepository.instance.user.name}!',
                              style: TextStyle (
                                  fontSize: 14
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding (
                      padding: EdgeInsets.only(top: 20),
                    ),
                    ListTile(
                      title: Text('Vire PRO'),
                      trailing: Icon(Icons.star, color: AppColors.appSecondaryColor,),
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => ProPage()),
                        );
                      },
                    ),
                    ListTile(
                      title: Text('Configurações'),
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => SettingsPage()),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
            bottomNavigationBar: BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              items: const <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(Icons.home),
                  title: Text('Home'),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.shopping_basket),
                  title: Text('Ofertas'),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.collections_bookmark),
                  title: Text('Coleções'),
                ),
              ],
              currentIndex: selectedBottomBarIndex,
              unselectedItemColor: Colors.grey,
              selectedItemColor: AppColors.appSecondaryColor,
              selectedFontSize: 12,
              onTap: onBottomBarItemTapped,
            ),
          ),
        )
      ],
    );
  }
}
