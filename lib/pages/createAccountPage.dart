import 'package:flutter/material.dart';
import 'package:pricetracker/app_configs/appConfigs.dart';
import 'package:pricetracker/components/borderedButton.dart';
import 'package:pricetracker/components/borderedTextFieldWithIcon.dart';
import 'package:pricetracker/components/clickableText.dart';
import 'package:pricetracker/components/defaultAlertDialog.dart';
import 'package:pricetracker/components/defaultAppBar.dart';
import 'package:pricetracker/components/loadingDialog.dart';
import 'package:pricetracker/design_configs/appColors.dart';
import 'package:pricetracker/design_configs/appDesignValues.dart';
import 'package:pricetracker/models/user.dart';
import 'package:pricetracker/pages/loginPage.dart';
import 'package:pricetracker/repositories/UserRepository.dart';
import 'package:pricetracker/validators/emailValidator.dart';
import 'package:pricetracker/validators/nameValidator.dart';
import 'package:pricetracker/validators/passwordValidator.dart';
import 'package:pricetracker/webservices/createAccountService.dart';

import 'bottomNavigationPage.dart';

class CreateAccountPage extends StatefulWidget {
  CreateAccountPage({Key key}) : super(key: key);

  @override
  _CreateAccountPageState createState() => _CreateAccountPageState();
}

class _CreateAccountPageState extends State<CreateAccountPage> {

  TextEditingController nameFieldController = new TextEditingController();
  TextEditingController emailFieldController = new TextEditingController();
  TextEditingController passwordFieldController = new TextEditingController();
  TextEditingController passwordConfirmationFieldController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
//    nameFieldController.text = "Djair";
//    emailFieldController.text = "djair.exemplo2@email.com";
//    passwordFieldController.text = "somepass";
//    passwordConfirmationFieldController.text = "somepass";
    return Scaffold(
      appBar: DefaultAppBar.build(),
      body: Center(
            child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(AppDesignValues.defaultPadding),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Center (
                      child: Text(
                        "Bem vindo!",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                            color: AppColors.appSecondaryColor),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: AppDesignValues.bigTopPadding),
                      child: BorderedTextFieldWithIcon("Nome", Icons.person, TextInputAction.next, (_) => {
                        FocusScope.of(context).nextFocus()
                      }, nameFieldController, TextInputType.text, false),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: AppDesignValues.normalTopPadding),
                      child: BorderedTextFieldWithIcon("Email", Icons.email, TextInputAction.next, (_) => {
                        FocusScope.of(context).nextFocus()
                      }, emailFieldController, TextInputType.emailAddress, false),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: AppDesignValues.normalTopPadding),
                      child: BorderedTextFieldWithIcon("Senha", Icons.vpn_key, TextInputAction.next, (_) => {
                        FocusScope.of(context).nextFocus()
                      }, passwordFieldController, TextInputType.text, true),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: AppDesignValues.normalTopPadding),
                      child: BorderedTextFieldWithIcon("Confirmar senha", Icons.vpn_key, TextInputAction.done, (_) => {
                        FocusScope.of(context).unfocus()
                      }, passwordConfirmationFieldController, TextInputType.text, true),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[BorderedButton("CADASTRAR-SE", ()  {

                        var nameValidator = NameValidator(nameFieldController.text);
                        var emailValidator = EmailValidator(emailFieldController.text);
                        var passwordValidator = PasswordValidator(passwordFieldController.text);

                        if (!nameValidator.validate()) {
                          DefaultAlertDialog("Campos inválidos", nameValidator.errorMessage).show(context);
                          return;
                        }

                        if (!emailValidator.validate()) {
                          DefaultAlertDialog("Campos inválidos", emailValidator.errorMessage).show(context);
                          return;
                        }

                        if (!passwordValidator.validate()) {
                          DefaultAlertDialog("Campos inválidos", passwordValidator.errorMessage).show(context);
                          return;
                        }

                        if (passwordConfirmationFieldController.text != passwordFieldController.text) {
                          DefaultAlertDialog("Campos inválidos", "A confirmação de senha e a senha devem ser a mesma.").show(context);
                          return;
                        }

                        LoadingDialog.show(context);
                        CreateAccountService().createAccount(nameFieldController.text, emailFieldController.text, passwordFieldController.text).then((user) {
                          Navigator.pop(context);
                          Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(builder: (context) => BottomNavigationPage()),
                                (Route<dynamic> route) => false,
                          );
                        }).catchError((error) {
                          Navigator.pop(context);
                          DefaultAlertDialog("Erro", error.toString()).show(context);
                        });

                      })],
                    ),
//                    Container(
//                      margin: EdgeInsets.only(top: AppDesignValues.mediumTopPadding),
//                      child: Text(
//                        "Ou cadastre-se usando:",
//                        style: TextStyle(color: AppColors.appSmallLabelColor),
//                      ),
//                    ),
//                    Container(
//                      margin: EdgeInsets.only(top: 10),
//                      child: Row(
//                        mainAxisAlignment: MainAxisAlignment.center,
//                        children: <Widget>[
//                          Container(
//                            margin: EdgeInsets.only(
//                                right: AppDesignValues.defaultPadding),
//                            child: Icon(Icons.face,
//                                size: 35, color: AppColors.appSecondaryColor),
//                          ),
//                          Icon(Icons.settings,
//                              size: 35, color: AppColors.appSecondaryColor),
//                        ],
//                      ),
//                    ),
                    Container(
                        margin: EdgeInsets.only(top: AppDesignValues.mediumTopPadding),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Já é cadastrado?",
                              style: TextStyle(
                                color: AppColors.appSmallLabelColor,
                                fontSize: AppDesignValues.actionTextFontSize
                              ),
                            ),
                            ClickableText("  Entre", () => {
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => LoginPage()),
                              )
                            })
                          ],
                        )),
                  ],
                ),
            )
        ),
      ),
    );
  }
}
