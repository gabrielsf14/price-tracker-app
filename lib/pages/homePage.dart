import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:pricetracker/components/addProductAlertDialog.dart';
import 'package:pricetracker/components/borderedButton.dart';
import 'package:pricetracker/components/circularNetworkImage.dart';
import 'package:pricetracker/components/defaultAlertDialog.dart';
import 'package:pricetracker/components/loadingDialog.dart';
import 'package:pricetracker/components/rectangularNetworkImage.dart';
import 'package:pricetracker/constants/routePaths.dart';
import 'package:pricetracker/design_configs/appColors.dart';
import 'package:pricetracker/design_configs/appDesignValues.dart';
import 'package:pricetracker/models/product.dart';
import 'package:pricetracker/models/site.dart';
import 'package:pricetracker/models/sortOption.dart';
import 'package:pricetracker/pages/productDetailPage.dart';
import 'package:pricetracker/repositories/UserRepository.dart';
import 'package:pricetracker/services/locator.dart';
import 'package:pricetracker/services/navigationService.dart';
import 'package:pricetracker/webservices/createProductService.dart';
import 'package:pricetracker/webservices/deleteProductService.dart';
import 'package:pricetracker/webservices/getProductsService.dart';
import 'package:pricetracker/webservices/loginService.dart';

class HomePage extends StatefulWidget {
  _HomePageState state;

  HomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomePageState createState() {
    state = _HomePageState();
    return state;
  }

  void reloadProductsList() {
    state.reloadProductsList();
  }
}

class _HomePageState extends State<HomePage> {
  var scrollController = ScrollController();

  var showErrorToLoadProducts = false;
  var showProgressBar = true;
  var showPaginationProgressBar = false;
  var paginationEnded = false;
  var loading = false;
  var showFilters = false;
  var showSortOptions = false;
  Site selectedSite;
  var currentQuery = "";

  var allProducts = List<Product>();
  var productsFiltered = List<Product>();
  var sites = List<Site>();

  var currentPage = 1;

  String sortOptionRadioValue = SortOption.date.getName();

  void getSites() {
    sites.clear();
    allProducts.forEach((product) {
      if (!sites.contains(product.site)) {
        sites.add(product.site);
      }
    });
  }

  void getProducts() {
    GetProductsService().getProducts(currentPage, "").then((products) {
      setState(() {
        if (products.isEmpty) {
          paginationEnded = true;
        }
        showPaginationProgressBar = false;
        showProgressBar = false;
        showErrorToLoadProducts = false;
        allProducts.addAll(products);
        allProducts.sort((p1, p2) =>
            p2.getUpdatedAtAsDate().compareTo(p1.getUpdatedAtAsDate()));
        productsFiltered = allProducts;
        getSites();
        loading = false;
      });
    }).catchError((error) {
      setState(() {
        currentPage = 1;
        showPaginationProgressBar = false;
        showErrorToLoadProducts = true;
        showProgressBar = false;
        loading = false;
      });
    });
  }

  Future<void> refreshProductsList() async {
    var products = await GetProductsService().getProducts(1, "");
    setState(() {
      currentPage = 1;
      showPaginationProgressBar = false;
      showProgressBar = false;
      showErrorToLoadProducts = false;
      paginationEnded = false;
      allProducts.clear();
      allProducts.addAll(products);
      allProducts.sort((p1, p2) =>
          p2.getUpdatedAtAsDate().compareTo(p1.getUpdatedAtAsDate()));
      productsFiltered = allProducts;
      getSites();
      loading = false;
    });
  }

  void reloadProductsList() {
    setState(() {
      currentPage = 1;
      allProducts.clear();
      productsFiltered.clear();
      showProgressBar = true;
      getProducts();
    });
  }

  void createProduct(String url) {
    LoadingDialog.show(context);
    CreateProductService().createProduct(url).then((product) {
      Navigator.pop(context);
      Navigator.pop(context);
      DefaultAlertDialog("Sucesso", "Seu produto foi cadastrado com sucesso!").show(context);
      reloadProductsList();
    }).catchError((error) {
      Navigator.pop(context);
      DefaultAlertDialog("Erro", error).show(context);
    });
  }

  void filterListByQuery(String query) {
    if (query.isEmpty) {
      productsFiltered = allProducts;
    } else {
      productsFiltered = allProducts
          .where((p) => p.title.toLowerCase().contains(query.toLowerCase()))
          .toList();
    }
  }

  void filterListBySite(Site site) {
    if (site != null) {
      productsFiltered = productsFiltered.where((p) => p.site == site).toList();
    }
  }

  void filter() {
    setState(() {
      filterListByQuery(currentQuery);
      filterListBySite(selectedSite);
    });
  }

  void sortList(SortOption sortOption) {
    switch (sortOption) {
      case SortOption.date:
        productsFiltered.sort((p1, p2) =>
            p2.getCreatedAtAsDate().compareTo(p1.getCreatedAtAsDate()));
        break;
      case SortOption.alphabetic:
        productsFiltered.sort((p1, p2) => p1.title.compareTo(p2.title));
        break;
      case SortOption.price:
        productsFiltered.sort((p1, p2) => p1
            .getCurrentPriceAsDouble()
            .compareTo(p2.getCurrentPriceAsDouble()));
        break;
    }
  }

  void setupScrollController() {
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        if (scrollController.position.pixels == 0) {
          debugPrint("top");
        } else {
          debugPrint("bottom");
          if (!paginationEnded && !loading) {
            loading = true;
            showPaginationProgressBar = true;
            setState(() {
              currentPage++;
              getProducts();
            });
          }
        }
      }
    });
  }

  @override
  void initState() {
    super.initState();
    setupScrollController();
    getProducts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 20, left: 20),
          child: Row(
            children: <Widget>[
              Expanded(
                child: Text(
                  "${productsFiltered.length} ITENS",
                  style: TextStyle(fontSize: 14),
                ),
              ),
              GestureDetector(
                onTap: () {
                  setState(() {
                    showSortOptions = !showSortOptions;
                  });
                },
                child: Container(
                  height: 35,
                  width: 35,
                  child: Icon(Icons.sort_by_alpha, size: 20),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: showSortOptions ? Colors.black12 : Colors.white,
                    boxShadow: !showSortOptions
                        ? [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 1,
                              blurRadius: 6,
                              offset:
                                  Offset(0, 3), // changes position of shadow
                            ),
                          ]
                        : [],
                  ),
                ),
              ),
              GestureDetector(
                  onTap: () {
                    setState(() {
                      showFilters = !showFilters;
                    });
                  },
                  child: Container(
                    margin: EdgeInsets.only(left: 10, right: 20),
                    height: 35,
                    width: 35,
                    child: Icon(Icons.filter_list, size: 20),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: showFilters ? Colors.black12 : Colors.white,
                      boxShadow: !showFilters
                          ? [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 1,
                                blurRadius: 6,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ]
                          : [],
                    ),
                  )),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 60),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Visibility(
                visible: showSortOptions,
                child: Padding(
                  padding: EdgeInsets.only(left: 20),
                  child: Text(
                    "Ordenar por:",
                    textAlign: TextAlign.start,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: new TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 13.0,
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: showSortOptions,
                child: Container(
                  height: 55,
                  child: ListView.builder(
                    padding: EdgeInsets.only(
                        top: 10, bottom: 10, left: 20, right: 20),
                    scrollDirection: Axis.horizontal,
                    itemCount: SortOption.values.length,
                    itemBuilder: (context, index) {
                      var sortOption = SortOption.values[index];
                      return InkWell(
                        child: Container(
                          margin: EdgeInsets.only(right: 15),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 1,
                                  blurRadius: 6,
                                  offset: Offset(
                                      0, 3), // changes position of shadow
                                ),
                              ]),
                          child: Row(
                            children: <Widget>[
                              Radio(
                                activeColor: Colors.black,
                                value: sortOption.getName(),
                                groupValue: sortOptionRadioValue,
                                onChanged: (val) {
                                  setState(() {
                                    sortOptionRadioValue = val;
                                    sortList(sortOption);
                                  });
                                },
                              ),
                              Text(
                                sortOption.getName(),
                                textAlign: TextAlign.start,
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                style: new TextStyle(
                                  fontSize: 12.0,
                                  color: Colors.black,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(right: 15),
                              ),
                            ],
                          ),
                        ),
                        onTap: () {
                          setState(() {
                            sortOptionRadioValue = sortOption.getName();
                            sortList(sortOption);
                          });
                        },
                      );
                    },
                  ),
                ),
              ),
              Visibility(
                visible: showFilters,
                child: Padding(
                  padding: EdgeInsets.only(left: 20),
                  child: Text(
                    "Lojas:",
                    textAlign: TextAlign.start,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: new TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 13.0,
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: showFilters,
                child: Container(
                  height: 55,
                  child: ListView.builder(
                    padding: EdgeInsets.only(
                        top: 10, bottom: 10, left: 20, right: 20),
                    scrollDirection: Axis.horizontal,
                    itemCount: sites.length,
                    itemBuilder: (context, index) {
                      var site = sites[index];
                      return InkWell(
                        child: Container(
                          margin: EdgeInsets.only(right: 15),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                              color: site != selectedSite
                                  ? Colors.white
                                  : Colors.black12,
                              boxShadow: site != selectedSite
                                  ? [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.5),
                                        spreadRadius: 1,
                                        blurRadius: 6,
                                        offset: Offset(
                                            0, 3), // changes position of shadow
                                      ),
                                    ]
                                  : []),
                          child: Row(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(left: 15),
                              ),
                              CircularNetworkImage(15, 15, site.image),
                              Padding(
                                padding: EdgeInsets.only(right: 5),
                              ),
                              Text(
                                site.name,
                                textAlign: TextAlign.start,
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                style: new TextStyle(
                                  fontSize: 12.0,
                                  color: Colors.black,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(right: 15),
                              ),
                            ],
                          ),
                        ),
                        onTap: () {
                          setState(() {
                            if (selectedSite == site) {
                              selectedSite = null;
                            } else {
                              selectedSite = site;
                            }
                            filter();
                          });
                        },
                      );
                    },
                  ),
                ),
              ),
              Expanded(
                  child: Visibility (
                    visible: !showErrorToLoadProducts,
                    child: RefreshIndicator(
                      backgroundColor: AppColors.appSecondaryColor,
                      child: ListView.builder(
                        padding: EdgeInsets.only(bottom: 60),
                        controller: scrollController,
                        itemCount: productsFiltered.length,
                        itemBuilder: (context, index) {
                          return InkWell(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ProductDetailPage(
                                          product: productsFiltered[index])),
                                ).then((deletedProductId) {
                                  if (deletedProductId != null) {
                                    setState(() {
                                      productsFiltered.removeWhere(
                                              (product) => product.id == deletedProductId);
                                    });
                                  }
                                });
                              },
                              child: ProductItem(
                                product: productsFiltered[index],
                                isLast: index == productsFiltered.length - 1,
                                homePageState: this,
                              ));
                        },
                      ),
                      onRefresh: refreshProductsList,
                    ),
                  )
              ),
              Center(
                child: Visibility(
                    visible: showPaginationProgressBar,
                    child: Padding(
                        padding: EdgeInsets.only(bottom: 10),
                        child: CircularProgressIndicator(
                            backgroundColor: AppColors.appSecondaryColor))),
              )
            ],
          ),
        ),
        Center(
            child: Visibility(
          visible: showProgressBar,
          child: CircularProgressIndicator(
              backgroundColor: AppColors.appSecondaryColor),
        )),
        Visibility(
            visible: showErrorToLoadProducts,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                      left: AppDesignValues.defaultPadding,
                      right: AppDesignValues.defaultPadding),
                  child: Text(
                    "Não foi possível carregar seus produtos, verifique sua conexão e tente novamente.",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 16, color: AppColors.appSecondaryColor),
                  ),
                ),
                BorderedButton(
                    "Tentar novamente",
                    () => {
                          setState(() {
                            showProgressBar = true;
                            showErrorToLoadProducts = false;
                            getProducts();
                          })
                        })
              ],
            )),
        Visibility(
            visible: !showProgressBar && !showErrorToLoadProducts && productsFiltered.isEmpty,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                      left: AppDesignValues.defaultPadding,
                      right: AppDesignValues.defaultPadding),
                  child: Text(
                    "Você ainda não tem nenhum produto, comece adicinando seu primeiro produto!",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 16, color: AppColors.appSecondaryColor),
                  ),
                ),
                BorderedButton(
                    "Adicionar produto",
                        () {
                        AddProductAlertDialog().show(context, (url) {
                          createProduct(url);
                        });
                    })
              ],
            ))
      ],
    ));
  }
}

class ProductItem extends StatelessWidget {
  final Product product;
  final bool isLast;
  final _HomePageState homePageState;

  ProductItem({Key key, this.product, this.isLast, this.homePageState})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Slidable(
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        CircularNetworkImage(20, 20, product.site.image),
                        Padding(
                          padding: EdgeInsets.only(left: 5),
                          child: Text(
                            product.site.name,
                            style: TextStyle(fontSize: 12),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 4, right: 10),
                      child: RectangularNetworkImage(100, 100, product.image),
                    ),
                  ],
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top: 23, right: 10),
                        child: Text(
                          product.title,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: new TextStyle(
                            fontSize: 12.0,
                            color: Colors.black,
                          ),
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(top: 10, right: 10),
                            child: Text(
                              "${product.currency} ${product.formattedCurrentPrice()}",
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: new TextStyle(
                                fontSize: 12.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                              ),
                            ),
                          ),
                          Column (
                            children: <Widget>[
                              Padding(padding: EdgeInsets.only(top: 7)),
                              Icon (
                                product.priceChangeIsPositive() ? Icons.trending_up : product.priceChangeIsZero() ? Icons.trending_flat : Icons.trending_down,
                                size: 15,
                                color: product.priceChangeIsPositive() ? Colors.red : product.priceChangeIsZero() ? Colors.amber : Colors.green,
                              ),
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(left: 3)),
                          Column (
                            children: <Widget>[
                              Padding(padding: EdgeInsets.only(top: 7)),
                              Text(product.getPriceChangeFormatted(),
                                style: TextStyle (
                                    color: product.priceChangeIsPositive() ? Colors.red : product.priceChangeIsZero() ? Colors.amber : Colors.green,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 12
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                      Visibility(
                        visible: product.label != null,
                        child: Container(
                          padding: EdgeInsets.only(
                              top: 5, bottom: 5, left: 10, right: 10),
                          margin: EdgeInsets.only(top: 10, right: 10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4),
                            color: product.label != null
                                ? product.label.getColorFromHex()
                                : Colors.white,
                          ),
                          child: Text(
                            product.label != null ? product.label.name : "",
                            style: TextStyle(fontSize: 12, color: Colors.white),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 14),
                  child:
                      Icon(Icons.navigate_next, size: 30, color: Colors.grey),
                ),
              ],
            ),
          ),
          Visibility(
            visible: !isLast,
            child: Container(
              height: 0.2,
              color: Colors.grey,
            ),
          )
        ],
      ),
      actionPane: SlidableDrawerActionPane(),
      actionExtentRatio: 0.12,
      secondaryActions: <Widget>[
        SlideAction(
          child: Icon(Icons.delete_forever_outlined,
              size: 30, color: AppColors.appDeleteButtonTextColor),
          color: AppColors.appDeleteButtonColor,
          onTap: () {
            LoadingDialog.show(context);
            DeleteProductService().deleteProduct(product).then((_) {
              Navigator.pop(context);
              var alert = DefaultAlertDialog(
                  "Sucesso!", "Seu produto foi deletado com sucesso!");
              alert.show(context);
              homePageState.setState(() {
                homePageState.productsFiltered.remove(product);
              });
            }).catchError((error) {
              Navigator.pop(context);
              DefaultAlertDialog("Erro", error.toString()).show(context);
            });
          },
        ),
      ],
    );
  }
}
