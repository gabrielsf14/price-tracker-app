import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:pricetracker/components/addProductAlertDialog.dart';
import 'package:pricetracker/components/borderedButton.dart';
import 'package:pricetracker/components/circularNetworkImage.dart';
import 'package:pricetracker/components/defaultAlertDialog.dart';
import 'package:pricetracker/components/loadingDialog.dart';
import 'package:pricetracker/components/rectangularNetworkImage.dart';
import 'package:pricetracker/design_configs/appColors.dart';
import 'package:pricetracker/design_configs/appDesignValues.dart';
import 'package:pricetracker/models/category.dart';
import 'package:pricetracker/models/lomaddeProduct.dart';
import 'package:pricetracker/models/product.dart';
import 'package:pricetracker/models/site.dart';
import 'package:pricetracker/models/sortOption.dart';
import 'package:pricetracker/models/store.dart';
import 'package:pricetracker/pages/productDetailPage.dart';
import 'package:pricetracker/repositories/UserRepository.dart';
import 'package:pricetracker/webservices/createProductService.dart';
import 'package:pricetracker/webservices/deleteProductService.dart';
import 'package:pricetracker/webservices/getProductsService.dart';
import 'package:pricetracker/webservices/loginService.dart';
import 'package:pricetracker/webservices/lomadde/getLomaddeProductsByCategoryService.dart';
import 'package:pricetracker/webservices/lomadde/getLomaddeProductsByQueryService.dart';
import 'package:pricetracker/webservices/lomadde/getLomaddeProductsByStoreService.dart';
import 'package:url_launcher/url_launcher.dart';

class LomaddeProductsPage extends StatefulWidget {

  final String title;
  final Store store;
  final Category category;
  final String query;

  _LomaddeProductsPageState state;

  LomaddeProductsPage({Key key, this.title, this.store, this.category, this.query}) : super(key: key);

  @override
  _LomaddeProductsPageState createState() {
    state = _LomaddeProductsPageState(store, category, query);
    return state;
  }
}

class _LomaddeProductsPageState extends State<LomaddeProductsPage> {

  var allProducts = List<LomaddeProduct>();
  Store store;
  Category category;
  String query;

  var scrollController = ScrollController();

  var showErrorToLoadProducts = false;
  var showProgressBar = true;
  var showPaginationProgressBar = false;
  var paginationEnded = false;
  var loading = false;
  var currentPage = 1;

  _LomaddeProductsPageState(this.store, this.category, this.query);

  void setupScrollController() {
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        if (scrollController.position.pixels == 0) {
          debugPrint("top");
        } else {
          debugPrint("bottom");
          if (!paginationEnded && !loading) {
            setState(() {
              loading = true;
              showPaginationProgressBar = true;
              currentPage++;
              getProducts();
            });
          }
        }
      }
    });
  }

  void getProductsByStore() {
    GetLomaddeProductsByStoreService().getProducts(currentPage, store).then((products) {
      setState(() {
        if (products.isEmpty) {
          paginationEnded = true;
        }
        showPaginationProgressBar = false;
        showProgressBar = false;
        showErrorToLoadProducts = false;
        allProducts.addAll(products);
        loading = false;
      });
    }).catchError((error) {
      setState(() {
        currentPage = 1;
        showPaginationProgressBar = false;
        showErrorToLoadProducts = true;
        showProgressBar = false;
        loading = false;
      });
    });
  }

  void getProductsByCategory() {
    GetLomaddeProductsByCategoryService().getProducts(currentPage, category).then((products) {
      setState(() {
        if (products.isEmpty) {
          paginationEnded = true;
        }
        showPaginationProgressBar = false;
        showProgressBar = false;
        showErrorToLoadProducts = false;
        allProducts.addAll(products);
        loading = false;
      });
    }).catchError((error) {
      setState(() {
        currentPage = 1;
        showPaginationProgressBar = false;
        showErrorToLoadProducts = true;
        showProgressBar = false;
        loading = false;
      });
    });
  }

  void getProductsByQuery() {
    GetLomaddeProductsByQueryService().getProducts(currentPage, query).then((products) {
      setState(() {
        if (products.isEmpty) {
          paginationEnded = true;
        }
        showPaginationProgressBar = false;
        showProgressBar = false;
        showErrorToLoadProducts = false;
        allProducts.addAll(products);
        loading = false;
      });
    }).catchError((error) {
      setState(() {
        currentPage = 1;
        showPaginationProgressBar = false;
        showErrorToLoadProducts = true;
        showProgressBar = false;
        loading = false;
      });
    });
  }

  void getProducts() {
    if (store != null) {
      getProductsByStore();
    } else if (category != null) {
      getProductsByCategory();
    } else {
      getProductsByQuery();
    }
  }

  @override
  void initState() {
    super.initState();
    setupScrollController();
    getProducts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Stack(
        children: [
          Visibility(
              visible: showProgressBar,
              child: Center(
                child: CircularProgressIndicator(backgroundColor: AppColors.appSecondaryColor),
              )
          ),
          Column(
            children: [
              Expanded(
                child: Visibility(
                  visible: !showErrorToLoadProducts,
                  child: ListView.builder(
                    padding: EdgeInsets.only(bottom: 60),
                    controller: scrollController,
                    itemCount: allProducts.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                          onTap: () async {
                            var url = allProducts[index].link;
                            if (await canLaunch(url)) {
                              launch(url);
                            }
                          },
                          child: ProductItem(
                            product: allProducts[index],
                            isLast: index == allProducts.length - 1,
                          )
                      );
                    },
                  ),
                ),
              ),
              Center(
                child: Visibility(
                    visible: showPaginationProgressBar,
                    child: Padding(
                        padding: EdgeInsets.only(bottom: 10),
                        child: CircularProgressIndicator(
                            backgroundColor: AppColors.appSecondaryColor))),
              )
            ],
          )
        ],
      )
    );
  }
}

class ProductItem extends StatelessWidget {
  final LomaddeProduct product;
  final bool isLast;

  ProductItem({Key key, this.product, this.isLast}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      CircularNetworkImage(20, 20, product.store.thumb),
                      Padding(
                        padding: EdgeInsets.only(left: 5),
                        child: Text(
                          product.store.name,
                          style: TextStyle(fontSize: 12),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 4, right: 10),
                    child: RectangularNetworkImage(100, 100, product.thumbnail),
                  ),
                ],
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 23, right: 10),
                      child: Text(
                        product.name,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: new TextStyle(
                          fontSize: 12.0,
                          color: Colors.black,
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 10, right: product.priceFrom != null ? 10 : 0),
                          child: Text(
                            product.priceFrom != null ? "R\$ ${product.formattedPriceFrom()}" : "",
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            style: new TextStyle(
                              fontSize: 12.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.black12,
                              decoration: TextDecoration.lineThrough,
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 10, right: 10),
                          child: Text(
                            "R\$ ${product.formattedPrice()}",
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            style: new TextStyle(
                              fontSize: 12.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10, right: 10),
                      child: Text(
                        product.discount > 0 ? "${product.discount.toInt()}% de desconto" : "",
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: new TextStyle(
                          fontSize: 14.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.green,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 14),
                child:
                Icon(Icons.navigate_next, size: 30, color: Colors.grey),
              ),
            ],
          ),
        ),
        Visibility(
          visible: !isLast,
          child: Container(
            height: 0.2,
            color: Colors.grey,
          ),
        )
      ],
    );
  }
}
