import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:pricetracker/components/borderedButton.dart';
import 'package:pricetracker/design_configs/appColors.dart';
import 'package:pricetracker/design_configs/appDesignValues.dart';
import 'package:pricetracker/models/label.dart';
import 'package:pricetracker/pages/proPage.dart';
import 'package:pricetracker/repositories/UserRepository.dart';
import 'package:pricetracker/webservices/getLabelsService.dart';

import 'collectionProductsPage.dart';

class CollectionsPage extends StatefulWidget {
  _CollectionsPageState state;

  CollectionsPage({Key key}) : super(key: key);

  @override
  _CollectionsPageState createState() {
    state = _CollectionsPageState();
    return state;
  }

  void reloadLabelsList() {
    state.reloadLabelsList();
  }
}

class _CollectionsPageState extends State<CollectionsPage> {

  var scrollController = ScrollController();

  var labels = new List<Label>();
  var showProgressBar = true;
  var showErrorToLoadCollections = false;
  var showPaginationProgressBar = false;
  var paginationEnded = false;
  var loading = false;
  var currentPage = 1;

  var userIsPro = false;

  void getLabels() {
    GetLabelsService().getLabels(currentPage).then((labels) {
      setState(() {
        showProgressBar = false;
        showPaginationProgressBar = false;
        showErrorToLoadCollections = false;
        loading = false;
        this.labels.addAll(labels);
        if (labels.isEmpty) {
          paginationEnded = true;
        }
      });
    }).catchError((error) {
      setState(() {
        currentPage = 1;
        showPaginationProgressBar = false;
        showErrorToLoadCollections = true;
        showProgressBar = false;
        loading = false;
      });
    });
  }

  void setupScrollController() {
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        if (scrollController.position.pixels == 0) {
          debugPrint("top");
        } else {
          debugPrint("bottom");
          if (!paginationEnded && !loading) {
            loading = true;
            showPaginationProgressBar = true;
            setState(() {
              currentPage++;
              getLabels();
            });
          }
        }
      }
    });
  }

  void reloadLabelsList() {
    setState(() {
      currentPage = 1;
      labels.clear();
      showProgressBar = true;
      getLabels();
    });
  }

  @override
  void initState() {
    super.initState();
    userIsPro = UserRepository.instance.user.isPro();
    setupScrollController();
    getLabels();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Visibility(
          visible: userIsPro,
          child: Column(
            children: [
              Expanded(
                child: ListView.builder(
                  controller: scrollController,
                  padding: EdgeInsets.only(bottom: 20, top: 10),
                  itemCount: labels.length,
                  itemBuilder: (context, index) {
                    var label = labels[index];
                    return InkWell(
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => CollectionProductsPage(title: label.name, label: label)));
                        },
                        child: CollectionItem(label: labels[index])
                    );
                  },
                ),
              ),
              Visibility(
                visible: showPaginationProgressBar,
                child: Container (
                  height: 55,
                  child: Padding(
                      padding: EdgeInsets.only(bottom: 10, top: 10),
                      child: CircularProgressIndicator(
                          backgroundColor: AppColors.appSecondaryColor)),
                ),
              )
            ],
          ),
        ),
        Center(
            child: Visibility(
              visible: showProgressBar && userIsPro,
              child: CircularProgressIndicator(
                  backgroundColor: AppColors.appSecondaryColor),
            )),
        Visibility(
            visible: !userIsPro,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                      left: AppDesignValues.defaultPadding,
                      right: AppDesignValues.defaultPadding),
                  child: Text(
                    "Vire PRO e organize seus produtos em coleções!",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 16, color: AppColors.appSecondaryColor),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                    children: [
                      Expanded(
                        child: BorderedButton(
                            "Virar PRO",
                                () => {
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => ProPage()),
                              )
                            }),
                      )
                    ],
                  ),
                )
              ],
            )),
        Visibility(
            visible: showErrorToLoadCollections && userIsPro,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                      left: AppDesignValues.defaultPadding,
                      right: AppDesignValues.defaultPadding),
                  child: Text(
                    "Não foi possível carregar seus produtos, verifique sua conexão e tente novamente.",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 16, color: AppColors.appSecondaryColor),
                  ),
                ),
                BorderedButton(
                    "Tentar novamente",
                        () => {
                      setState(() {
                        showProgressBar = true;
                        showErrorToLoadCollections = false;
                        getLabels();
                      })
                    })
              ],
            )),
        Center(
          child: Visibility(
              visible: !showProgressBar && !showErrorToLoadCollections && labels.isEmpty && userIsPro,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Você ainda não tem nenhuma coleção.",
                    style: TextStyle(
                        fontSize: 16,
                        color: AppColors.appSecondaryColor),
                  ),
                ],
              )),
        ),
      ],
    );
  }
}

class CollectionItem extends StatelessWidget {
  
  final Label label;

  CollectionItem({Key key, this.label}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 90,
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration (
        borderRadius: BorderRadius.circular(5),
        color: label.getColorFromHex().withOpacity(0.5),
      ),
      child: Column (
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Row (
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Icon(Icons.collections_bookmark, color: label.getColorFromHex(), size: 43),
                Expanded(
                  child: Text(""),
                ),
                Icon(Icons.navigate_next, color: label.getColorFromHex(), size: 30),
              ],
            ),
          ),
          Padding(padding: EdgeInsets.only(top: 4)),
          Text(
            label.name,
            style: TextStyle(
              color: label.getColorFromHex(),
              fontWeight: FontWeight.bold,
              fontSize: 16
            ),
          )
        ],
      ),
    );
  }
}