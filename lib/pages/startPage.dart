import 'package:flutter/material.dart';
import 'package:pricetracker/app_configs/appConfigs.dart';
import 'package:pricetracker/components/borderedButton.dart';
import 'package:pricetracker/components/borderedTextFieldWithIcon.dart';
import 'package:pricetracker/components/defaultAlertDialog.dart';
import 'package:pricetracker/components/defaultAppBar.dart';
import 'package:pricetracker/design_configs/appColors.dart';
import 'package:pricetracker/design_configs/appDesignValues.dart';
import 'package:pricetracker/pages/createAccountPage.dart';
import 'package:pricetracker/validators/emailValidator.dart';

import 'loginPage.dart';

class StartPage extends StatefulWidget {
  StartPage({Key key}) : super(key: key);

  @override
  _StartPageState createState() => _StartPageState();
}

class _StartPageState extends State<StartPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding (
        padding: EdgeInsets.only(left: 20, right: 20),
        child: Center (
          child: Column (
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Center (
                child: Text(
                  AppConfigs.appName,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: AppColors.appSecondaryColor),
                ),
              ),
              Padding (
                padding: EdgeInsets.only(top: 50),
              ),
              Center (
                  child: Container(
                      width: 87,
                      height: 87,
                      decoration: new BoxDecoration(
                          shape: BoxShape.circle,
                          image: new DecorationImage(
                              fit: BoxFit.fill,
                              image: AssetImage("assets/placeholder.png")
                          )
                      )
                  )
              ),
              Padding (
                padding: EdgeInsets.only(top: 50),
              ),
              Center (
                child: Text(
                  "A expressão Lorem ipsum em design gráfico e editoração é um texto padrão em latim utilizado na produção gráfica.",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 18,
                      color: AppColors.appSecondaryColor),
                ),
              ),
              Padding (
                padding: EdgeInsets.only(top: 120),
              ),
              Row (
                children: <Widget>[
                  Expanded (
                    child: Container(
                      height: 45,
                        margin: EdgeInsets.only(top: AppDesignValues.normalTopPadding),
                        child: FlatButton(
                            color: AppColors.appSecondaryColor,
                            textColor: Colors.white,
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(5)
                            ),
                            child: Text("ENTRAR"),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => LoginPage()),
                              );
                            }
                        )
                    )
                  )
                ],
              ),
              Row (
                children: <Widget>[
                  Expanded (
                      child: Container(
                          height: 45,
                          margin: EdgeInsets.only(top: 15),
                          decoration: BoxDecoration (
                            border: Border.all(width: 1, color: Colors.black),
                            borderRadius: BorderRadius.all(Radius.circular(5))
                          ),
                          child: FlatButton(
                              color: Colors.white,
                              textColor: Colors.black,
                              child: Text("CADASTRAR-SE"),
                              shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(5)
                              ),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => CreateAccountPage()),
                                );
                              }
                          )
                      )
                  )
                ],
              )
            ],
          ),
        )
      )
    );
  }
}
