import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_inapp_purchase/flutter_inapp_purchase.dart';
import 'package:pricetracker/design_configs/appColors.dart';
import 'package:pricetracker/pages/bottomNavigationPage.dart';
import 'package:pricetracker/repositories/UserRepository.dart';
import 'package:pricetracker/webservices/updatePurchaseTokenService.dart';

import '../main.dart';

class ProPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return new ProPageState();
  }
}

class ProPageState extends State<ProPage> {

  //inapp-purchases
  StreamSubscription purchaseUpdatedSubscription;
  StreamSubscription purchaseErrorSubscription;
  StreamSubscription conectionSubscription;
//  final List<String> productList = ['android.test.purchased'];
  final List<String> productList = ['getlist.subscription.mounthly1'];

  List<IAPItem> items = [];
  //inapp-purchases

  var userIsPro = false;

  @override
  void initState() {
    super.initState();
    userIsPro = UserRepository.instance.user.isPro();
    initPlatformState();
  }

  @override
  void dispose() {
    if (conectionSubscription != null) {
      conectionSubscription.cancel();
      conectionSubscription = null;
    }
    super.dispose();
  }

  //inapp-purchases
  Future<void> initPlatformState() async {
    String platformVersion;
    try {
      platformVersion = await FlutterInappPurchase.instance.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    var result = await FlutterInappPurchase.instance.initConnection;
    print('result: $result');

    if (!mounted) return;

    try {
      String msg = await FlutterInappPurchase.instance.consumeAllItems;
      print('consumeAllItems: $msg');
    } catch (err) {
      print('consumeAllItems error: $err');
    }

    conectionSubscription = FlutterInappPurchase.connectionUpdated.listen((connected) {
      print('connected: $connected');
    });

    purchaseUpdatedSubscription = FlutterInappPurchase.purchaseUpdated.listen((productItem) {
      print('purchase token: ${productItem.purchaseToken}');
      UpdatePurchaseTokenService().updatePurchaseToken(productItem.purchaseToken).then((value) {
        Navigator.pop(context);
        Navigator.pushReplacement(
          context,
          PageRouteBuilder(
            transitionDuration: Duration.zero,
            pageBuilder: (_, __, ___) => BottomNavigationPage(),
          ),
        );
      });
    });

    purchaseErrorSubscription = FlutterInappPurchase.purchaseError.listen((purchaseError) {
      print('purchase-error: $purchaseError');
    });

    await getProducts();
  }

  void requestPurchase(IAPItem item) {
    FlutterInappPurchase.instance.requestSubscription(item.productId);
//    FlutterInappPurchase.instance.requestPurchase(item.productId);
  }

  Future getProducts() async {
    List<IAPItem> items = await FlutterInappPurchase.instance.getSubscriptions(productList);
//    List<IAPItem> items = await FlutterInappPurchase.instance.getProducts(productList);

    for (var item in items) {
      print('${item.toString()}');
      this.items.add(item);
    }
  }
  //inapp-purchases

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Vire PRO"),
      ),
      body: Padding(
        padding: EdgeInsets.only(left: 20, right: 20, top: 20),
        child: Column (
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Vire PRO e tenha acesso a todas as vantagens do aplicativo!",
              style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 18,
                  color: AppColors.appSecondaryColor
              ),
            ),
            Padding(padding: EdgeInsets.only(top:20)),
            Text(
              "- Crie coleções para organizar seus produtos.",
              style: TextStyle(
                  fontSize: 16,
                  color: AppColors.appSecondaryColor
              ),
            ),
            Padding(padding: EdgeInsets.only(top:20)),
            Text(
              "- Explore os produtos separadamente em cada coleção.",
              style: TextStyle(
                  fontSize: 16,
                  color: AppColors.appSecondaryColor
              ),
            ),
            Padding(padding: EdgeInsets.only(top:20)),
            Text(
              "- Visualize de forma gráfica o histórico de preços de seus produtos.",
              style: TextStyle(
                  fontSize: 16,
                  color: AppColors.appSecondaryColor
              ),
            ),
            Padding(padding: EdgeInsets.only(top:30)),
            Row(
              children: [
                Expanded(
                  child: FlatButton(
                  color: AppColors.appSecondaryColor,
                  textColor: Colors.white,
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(5)
                  ),
                  child: !userIsPro ? Text("Virar PRO - R\$ 4,99") : Text("Você ja é PRO :)"),
                  onPressed: !userIsPro ? () {
                    requestPurchase(items[0]);
                  } : (){}),
                )
              ],
            )
          ],
        ),
      )
    );
  }
}