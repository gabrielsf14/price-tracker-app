import 'package:flutter/material.dart';
import 'package:pricetracker/components/borderedButton.dart';
import 'package:pricetracker/components/borderedTextFieldWithIcon.dart';
import 'package:pricetracker/components/defaultAlertDialog.dart';
import 'package:pricetracker/components/defaultAppBar.dart';
import 'package:pricetracker/components/loadingDialog.dart';
import 'package:pricetracker/design_configs/appColors.dart';
import 'package:pricetracker/design_configs/appDesignValues.dart';
import 'package:pricetracker/validators/emailValidator.dart';
import 'package:pricetracker/webservices/recoverPasswordService.dart';

class RecoverEmailPage extends StatefulWidget {
  RecoverEmailPage({Key key}) : super(key: key);

  @override
  _RecoverEmailPageState createState() => _RecoverEmailPageState();
}

class _RecoverEmailPageState extends State<RecoverEmailPage> {

  TextEditingController emailFieldController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar.build(),
      body: Center(
        child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.all(AppDesignValues.defaultPadding),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Center (
                    child: Text(
                      "Digite seu email",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          color: AppColors.appSecondaryColor),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 100),
                    child: BorderedTextFieldWithIcon("Email", Icons.email, TextInputAction.done, (_) => {
                      FocusScope.of(context).nextFocus()
                    }, emailFieldController, TextInputType.emailAddress, false),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[BorderedButton("RECUPERAR", () {
                      var emailValidator = EmailValidator(emailFieldController.text);
                      if (emailValidator.validate()) {
                        LoadingDialog.show(context);
                        RecoverPasswordService().recoverPassword(emailFieldController.text).then((_) {
                          Navigator.pop(context);
                          DefaultAlertDialog("Sucesso", "Uma mensagem de recuperação foi enviada para seu email.").show(context);
                        }).catchError((error) {
                          Navigator.pop(context);
                          DefaultAlertDialog("Erro", error.toString()).show(context);
                        });
                      } else {
                        DefaultAlertDialog("Campos inválidos", emailValidator.errorMessage).show(context);
                      }
                    })],
                  ),
                ],
              ),
            )
        ),
      ),
    );
  }
}
