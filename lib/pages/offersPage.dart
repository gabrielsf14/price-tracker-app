import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pricetracker/components/borderedButton.dart';
import 'package:pricetracker/components/rectangularNetworkImage.dart';
import 'package:pricetracker/design_configs/appColors.dart';
import 'package:pricetracker/design_configs/appDesignValues.dart';
import 'package:pricetracker/models/category.dart';
import 'package:pricetracker/models/store.dart';
import 'package:pricetracker/webservices/lomadde/getCategoriesService.dart';
import 'package:pricetracker/webservices/lomadde/getStoresService.dart';

import 'lomaddeProductsPage.dart';

class OffersPage extends StatefulWidget {
  _OffersPageState state;

  OffersPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _OffersPageState createState() {
    state = _OffersPageState();
    return state;
  }
}

class _OffersPageState extends State<OffersPage> {

  var stores = List<Store>();
  var storesLoaded = false;
  var errorToLoadStores = false;

  var categories = List<Category>();
  var categoriesLoaded = false;
  var errorToLoadCategories = false;

  void getStores() {
    GetStoresService().getStores().then((stores) {
      setState(() {
        this.stores = stores;
        storesLoaded = true;
      });
    }).catchError((error) {
      setState(() {
        errorToLoadStores = true;
        storesLoaded = true;
      });
    });
  }

  void getCategories() {
    GetCategoriesService().getCategories().then((categories) {
      setState(() {
        this.categories = categories;
        categoriesLoaded = true;
      });
    }).catchError((error) {
      setState(() {
        errorToLoadCategories = true;
        categoriesLoaded = true;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getStores();
    getCategories();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(left: 20, top: 20, bottom: 20),
              child: Text(
                "Lojas",
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w600
                ),
              ),
            ),
            Container (
              height: 120,
              child: Stack(
                children: [
                  Visibility(
                      visible: errorToLoadStores,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(
                                left: AppDesignValues.defaultPadding,
                                right: AppDesignValues.defaultPadding),
                            child: Text(
                              "Não foi possível carregar as lojas, verifique sua conexão e tente novamente.",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 16, color: AppColors.appSecondaryColor),
                            ),
                          ),
                          BorderedButton(
                              "Tentar novamente",
                                  () => {
                                setState(() {
                                  errorToLoadStores = false;
                                  storesLoaded = false;
                                  getStores();
                                })
                              })
                        ],
                      )
                  ),
                  Visibility(
                    visible: !storesLoaded,
                    child: Center(
                      child: CircularProgressIndicator(backgroundColor: AppColors.appSecondaryColor),
                    )
                  ),
                  Visibility(
                    visible: storesLoaded && !errorToLoadStores,
                    child: Container(
                      child: ListView.builder(
                        padding: EdgeInsets.only(
                            top: 10, bottom: 10, left: 20, right: 20),
                        scrollDirection: Axis.horizontal,
                        itemCount: stores.length,
                        itemBuilder: (context, index) {
                          var store = stores[index];
                          return InkWell(
                            child: Container(
                              height: 110,
                              width: 75,
                              margin: EdgeInsets.only(right: 15),
                              decoration: BoxDecoration(
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(3)),
                                  color: Colors.white,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      spreadRadius: 1,
                                      blurRadius: 6,
                                      offset: Offset(
                                          0, 3), // changes position of shadow
                                    ),
                                  ]),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  RectangularNetworkImage(50, 50, store.thumb),
                                  Text(
                                    store.name,
                                    maxLines: 2,
                                    textAlign: TextAlign.center,
                                    style: TextStyle (
                                      fontSize: 12,
                                      fontWeight: FontWeight.w600
                                    ),
                                  )
                                ],
                              ),
                            ),
                            onTap: () {
                              setState(() {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => LomaddeProductsPage(title: store.name, store: store)),
                                );
                              });
                            },
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 20, top: 40, bottom: 20),
              child: Text(
                "Categorias",
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w600
                ),
              ),
            ),
            Visibility(
                visible: errorToLoadCategories,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(
                          left: AppDesignValues.defaultPadding,
                          right: AppDesignValues.defaultPadding),
                      child: Text(
                        "Não foi possível carregar as categorias, verifique sua conexão e tente novamente.",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 16, color: AppColors.appSecondaryColor),
                      ),
                    ),
                    BorderedButton(
                        "Tentar novamente",
                            () => {
                          setState(() {
                            errorToLoadCategories = false;
                            categoriesLoaded = false;
                            getCategories();
                          })
                        })
                  ],
                )
            ),
            Visibility(
              visible: !categoriesLoaded,
              child: Center(
                child: CircularProgressIndicator(backgroundColor: AppColors.appSecondaryColor),
              )
            ),
            Visibility(
              visible: categoriesLoaded && !errorToLoadCategories,
              child: Expanded(
                child: ListView.builder(
                  itemCount: categories.length,
                  itemBuilder: (context, index) {
                    var category = categories[index];
                    return InkWell(
                      child: Padding (
                        padding: EdgeInsets.only(top: 10, left: 20, right: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: [
                                Expanded(
                                  child: Text(
                                    category.name,
                                    style: TextStyle (
                                        fontSize: 15,
                                        fontWeight: FontWeight.w600
                                    ),
                                  ),
                                ),
                                Icon(Icons.navigate_next, size: 30, color: Colors.black),
                              ],
                            ),
                            Padding(padding: EdgeInsets.only(bottom: 10)),
                            Container(height: 1, color: Colors.black38,)
                          ],
                        ),
                      ),
                      onTap: () {
                        setState(() {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LomaddeProductsPage(title: category.name, category: category)),
                          );
                        });
                      },
                    );
                  },
                ),
              ),
            )
          ],
        ),
    );
  }
}