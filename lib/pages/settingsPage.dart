import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:pricetracker/components/customTrackShape.dart';
import 'package:pricetracker/components/loadingDialog.dart';
import 'package:pricetracker/components/optionsAlertDialog.dart';
import 'package:pricetracker/design_configs/appColors.dart';
import 'package:pricetracker/models/userPrefs.dart';
import 'package:pricetracker/pages/startPage.dart';
import 'package:pricetracker/repositories/UserRepository.dart';
import 'package:pricetracker/webservices/getUserPrefsService.dart';
import 'package:pricetracker/webservices/updateUserPrefsService.dart';
import 'package:url_launcher/url_launcher.dart';

class SettingsPage extends StatefulWidget {
  SettingsPage({Key key}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  var notifyPriceDrop = false;
  var sliderValue = 0.0;

  void loadUserPrefs() {
    LoadingDialog.show(context);
    GetUserPrefsService().getPrefs().then((userPrefs) {
      setState(() {
        sliderValue = double.parse(userPrefs.rate);
        notifyPriceDrop = userPrefs.enabled;
        Navigator.pop(context);
      });
    }).catchError((error) {
      Navigator.pop(context);
    });
  }

  void updateUserPrefs() {
    LoadingDialog.show(context);
    UpdateUserPrefsService().updatePrefs(UserPrefs(notifyPriceDrop, (sliderValue/100).toString())).then((_) {
      Navigator.pop(context);
    }).catchError((error) {
      Navigator.pop(context);
    });
  }

  Future getThingsOnStartup() async {
    await Future.delayed(Duration(milliseconds: 200));
  }

  @override
  void initState() {
    getThingsOnStartup().then((value){
      loadUserPrefs();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Configurações"),
        ),
        body: Column(
          children: [
            Padding(
                padding: EdgeInsets.only(top: 30, left: 20, right: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      "Notificações",
                      style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                    ),
//                    Row(
//                      children: [
//                        Icon(Icons.notifications_none),
//                        Padding(
//                          padding: EdgeInsets.only(left: 15),
//                        ),
//                        Expanded(
//                          child: Text("Receber ofertas"),
//                        ),
//                        Switch(
//                          value: receiveOffers,
//                          onChanged: (value) {
//                            setState(() {
//                              receiveOffers = value;
//                            });
//                          },
//                          activeTrackColor: Colors.green.withOpacity(0.5),
//                          activeColor: Colors.green,
//                          inactiveTrackColor: Colors.red.withOpacity(0.5),
//                          inactiveThumbColor: Colors.red,
//                        )
//                      ],
//                    ),
                    Row(
                      children: [
                        Icon(Icons.notifications_none),
                        Padding(
                          padding: EdgeInsets.only(left: 15),
                        ),
                        Expanded(
                          child: Text("Aviso de queda de preço"),
                        ),
                        Switch(
                          value: notifyPriceDrop,
                          onChanged: (value) {
                            setState(() {
                              notifyPriceDrop = value;
                              updateUserPrefs();
                            });
                          },
                          activeTrackColor: Colors.green.withOpacity(0.5),
                          activeColor: Colors.green,
                          inactiveTrackColor: Colors.red.withOpacity(0.5),
                          inactiveThumbColor: Colors.red,
                        )
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 15),
                    ),
                    Text(
                      "Notificar quando a variação for maior que:",
                      style: TextStyle(
                        color: Colors.grey
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10),
                    ),
                    Row (
                      children: [
                        Expanded(
                          child: Text("0%"),
                        ),
                        Text("100%"),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 12, right: 12),
                      child: SliderTheme(
                        data: SliderTheme.of(context).copyWith(
                          activeTrackColor: AppColors.appSecondaryColor,
                          inactiveTrackColor: AppColors.appSecondaryColor,
                          trackShape: CustomTrackShape(),
                          trackHeight: 3.0,
                          thumbColor: AppColors.appSecondaryColor,
                          thumbShape: RoundSliderThumbShape(enabledThumbRadius: 10.0),
                          overlayColor: AppColors.appSecondaryColor.withAlpha(32),
                          overlayShape: RoundSliderOverlayShape(overlayRadius: 28.0),
                          valueIndicatorShape: RectangularSliderValueIndicatorShape(),
                          valueIndicatorColor: Colors.white,
                          valueIndicatorTextStyle: TextStyle(
                            color: AppColors.appSecondaryColor,
                          ),
                        ),
                        child: Slider(
                          value: sliderValue,
                          min: 0,
                          max: 100,
                          divisions: 10,
                          label: '${sliderValue.toInt()}%',
                          onChanged: (value) {
                            setState(() {
                              sliderValue = value;
                            });
                          },
                          onChangeEnd: (value) {
                            setState(() {
                              sliderValue = value;
                              updateUserPrefs();
                            });
                          },
                        ),
                      ),
                    )
                  ],
                )
            ),
            Container(
              color: Colors.black12,
              height: 1,
            ),
            GestureDetector(
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Row(
                  children: [
                    Icon(Icons.feedback_outlined),
                    Padding(
                      padding: EdgeInsets.only(left: 15),
                    ),
                    Expanded(
                      child: Text("Feedback"),
                    ),
                    Icon(Icons.navigate_next, size: 30, color: Colors.grey),
                  ],
                ),
              ),
              onTap: () async {
                var url = "market://details?id=br.com.getlist";
                if (await canLaunch(url)) {
                  launch(url);
                }
              },
            ),
            Container(
              color: Colors.black12,
              height: 1,
            ),
            Padding(
              padding: EdgeInsets.all(20),
              child: Row(
                children: [
                  Icon(Icons.privacy_tip_outlined),
                  Padding(
                    padding: EdgeInsets.only(left: 15),
                  ),
                  Expanded(
                    child: Text("Termos e condições"),
                  ),
                  Icon(Icons.navigate_next, size: 30, color: Colors.grey),
                ],
              ),
            ),
            Container(
              color: Colors.black12,
              height: 1,
            ),
            GestureDetector(
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Row(
                  children: [
                    Icon(Icons.logout),
                    Padding(
                      padding: EdgeInsets.only(left: 15),
                    ),
                    Expanded(
                      child: Text("Log out"),
                    ),
                  ],
                ),
              ),
              onTap: () {
                var dialog = OptionsAlertDialog("Log Out", "Deseja mesmo sair?");
                dialog.onAction = () {
                  UserRepository.instance.clear();
                  Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (context) => StartPage()),
                        (Route<dynamic> route) => false,
                  );
                };
                dialog.show(context);
              },
            )
          ],
        )
    );
  }
}
