import 'package:flutter/material.dart';
import 'package:pricetracker/components/borderedButton.dart';
import 'package:pricetracker/components/borderedTextFieldWithIcon.dart';
import 'package:pricetracker/components/clickableText.dart';
import 'package:pricetracker/components/defaultAlertDialog.dart';
import 'package:pricetracker/components/defaultAppBar.dart';
import 'package:pricetracker/components/loadingDialog.dart';
import 'package:pricetracker/design_configs/appColors.dart';
import 'package:pricetracker/design_configs/appDesignValues.dart';
import 'package:pricetracker/models/user.dart';
import 'package:pricetracker/pages/homePage.dart';
import 'package:pricetracker/pages/recoverEmailPage.dart';
import 'package:pricetracker/repositories/UserRepository.dart';
import 'package:pricetracker/storage/SharedPrefsStorage.dart';
import 'package:pricetracker/validators/emailValidator.dart';
import 'package:pricetracker/validators/passwordValidator.dart';
import 'package:pricetracker/webservices/loginService.dart';

import 'bottomNavigationPage.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  TextEditingController emailFieldController = new TextEditingController();
  TextEditingController passwordFieldController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
//    emailFieldController.text = "djair.exemplo2@email.com";
//    passwordFieldController.text = "somepass";
    return Scaffold(
      appBar: DefaultAppBar.build(),
      body: Center(
            child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(AppDesignValues.defaultPadding),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Center (
                      child: Text(
                        "Olá!",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                            color: AppColors.appSecondaryColor),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 100),
                      child: BorderedTextFieldWithIcon("Email", Icons.email, TextInputAction.next, (_) => {
                        FocusScope.of(context).nextFocus()
                      }, emailFieldController, TextInputType.emailAddress, false),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: AppDesignValues.normalTopPadding),
                      child: BorderedTextFieldWithIcon("Senha", Icons.vpn_key, TextInputAction.done, (_) => {
                        FocusScope.of(context).unfocus()
                      }, passwordFieldController, TextInputType.text, true),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[BorderedButton("ENTRAR", () {
                        var emailValidator = EmailValidator(emailFieldController.text);
                        if (!emailValidator.validate()) {
                          DefaultAlertDialog("Campos inválidos", emailValidator.errorMessage).show(context);
                          return;
                        }
                        LoadingDialog.show(context);
                        LoginService().login(emailFieldController.text, passwordFieldController.text).then((user) {
                          Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(builder: (context) => BottomNavigationPage()),
                                (Route<dynamic> route) => false,
                          );
                        }).catchError((error) {
                          Navigator.pop(context);
                          DefaultAlertDialog("Erro", error.toString()).show(context);
                        });
                      })],
                    ),
//                    Container(
//                      margin: EdgeInsets.only(top: AppDesignValues.mediumTopPadding),
//                      child: Text(
//                        "Ou entre usando:",
//                        style: TextStyle(color: AppColors.appSmallLabelColor),
//                      ),
//                    ),
//                    Container(
//                      margin: EdgeInsets.only(top: 10),
//                      child: Row(
//                        mainAxisAlignment: MainAxisAlignment.center,
//                        children: <Widget>[
//                          Container(
//                            margin: EdgeInsets.only(
//                                right: AppDesignValues.defaultPadding),
//                            child: Icon(Icons.face,
//                                size: 35, color: AppColors.appSecondaryColor),
//                          ),
//                          Icon(Icons.settings,
//                              size: 35, color: AppColors.appSecondaryColor),
//                        ],
//                      ),
//                    ),
                    Container(
                        margin: EdgeInsets.only(top: AppDesignValues.mediumTopPadding),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Esqueceu sua senha?",
                              style: TextStyle(
                                  color: AppColors.appSmallLabelColor,
                                  fontSize: AppDesignValues.actionTextFontSize
                              ),
                            ),
                            ClickableText("  Recupere", () => {
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => RecoverEmailPage()),
                              )
                            })
                          ],
                        )),
                  ],
                ),
            )
        ),
      ),
    );
  }
}
