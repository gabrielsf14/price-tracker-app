import 'package:flutter/material.dart';
import 'package:pricetracker/design_configs/appColors.dart';
import 'package:pricetracker/pages/bottomNavigationPage.dart';
import 'package:pricetracker/pages/createAccountPage.dart';
import 'package:pricetracker/pages/productDetailPage.dart';
import 'package:pricetracker/pages/startPage.dart';
import 'package:pricetracker/repositories/UserRepository.dart';
import 'package:pricetracker/services/locator.dart';
import 'package:pricetracker/services/navigationService.dart';
import 'package:pricetracker/storage/SharedPrefsStorage.dart';
import 'package:pricetracker/services/pushNotificationsManager.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'app_configs/appConfigs.dart';
import 'constants/routePaths.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SharedPrefsStorage.init();
  loadAppData();
  setupLocator();
  await PushNotificationsManager().init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context)  {
    return MaterialApp(
      navigatorKey: locator<NavigationService>().navigatorKey,
      routes: <String, WidgetBuilder>{
        ProductDetailRoute: (BuildContext context) => new ProductDetailPage()
      },
      title: AppConfigs.appName,
      theme: ThemeData(
          primarySwatch: AppColors.appThemeMaterialColor,
          cursorColor: AppColors.appSecondaryColor,
          textSelectionHandleColor: AppColors.appSecondaryColor
      ),
      home: userExists() ? BottomNavigationPage() : StartPage(),
    );
  }
}

void loadAppData()  {
  UserRepository.instance.load();
}

bool userExists() {
  return UserRepository.instance.userExists();
}