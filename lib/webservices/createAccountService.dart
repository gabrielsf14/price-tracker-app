import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pricetracker/models/user.dart';
import 'package:pricetracker/repositories/UserRepository.dart';
import 'package:pricetracker/webservices/webServiceSettings.dart';

class CreateAccountService {

  Future<User> createAccount(String name, String email, String password) async {
    final http.Response response = await http.post(
      '${WebServiceSettings.apiBase}/auth/signup',
      headers: WebServiceSettings.getHeaders(false),
      body: jsonEncode(<String, String>{
        'email': email,
        'name': name,
        'password': password,
        'passwordConfirmation': password,
      }),
    );
    if (response.statusCode == 201) {
      var user = User.fromJson(json.decode(response.body));
      user.password = password;
      UserRepository.instance.saveUser(user);
      return user;
    } else if (response.statusCode == 409) {
      return Future.error("Este endereço de email já está em uso.");
    } else {
      return Future.error("Não foi possível criar a conta, verifique sua conexão e tente novamente.");
    }
  }
}