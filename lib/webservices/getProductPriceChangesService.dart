import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pricetracker/models/priceChange.dart';
import 'package:pricetracker/models/product.dart';
import 'package:pricetracker/webservices/webServiceSettings.dart';

class GetProductPriceChangesService {

  Future<List<PriceChange>> getPriceChanges(String productId) async {
    final http.Response response = await http.get(
        '${WebServiceSettings.apiBase}/products/$productId',
        headers: WebServiceSettings.getHeaders(true)
    );
    if (response.statusCode == 200) {
      Map<String, dynamic> jsonMap = json.decode(response.body);
      Iterable list = jsonMap['priceChanges'];
      var changes = list.map((model) => PriceChange.fromJson(model)).toList();
      return changes;
    } else {
      return Future.error("Não foi possível obter os produtos.");
    }
  }
}