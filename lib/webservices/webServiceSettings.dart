import 'package:pricetracker/repositories/UserRepository.dart';

class WebServiceSettings {

  static final String apiBase =  'https://getlist-294913.uc.r.appspot.com';
  static final String lomaddeApiBase =  'http://sandbox-api.lomadee.com/v3/159530243956608872d00';

  static Map<String, String> getHeaders(bool needsAuth) {
    var headers = <String, String> {
      'Content-Type': 'application/json',
    };
    if (needsAuth) {
      headers["Authorization"] = "Bearer ${UserRepository.instance.user.token}";
    }
    return headers;
  }
}