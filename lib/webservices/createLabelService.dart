import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pricetracker/models/label.dart';
import 'package:pricetracker/webservices/webServiceSettings.dart';

class CreateLabelService {
  Future<Label> createLabel(String color, String name) async {
    final http.Response response = await http.post(
      '${WebServiceSettings.apiBase}/labels',
      headers: WebServiceSettings.getHeaders(true),
      body: jsonEncode(<String, String>{
        "name": name,
        "color": color
      }),
    );
    if (response.statusCode == 201) {
      var body = response.body;
      var labelMap = json.decode(body);
      return Label.fromJson(labelMap);
    } else {
      return Future.error("Não foi possível criar a coleção, verifique sua conexão e tente novamente.");
    }
  }
}