import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pricetracker/models/user.dart';
import 'package:pricetracker/webservices/webServiceSettings.dart';

class RecoverPasswordService {

  Future<void> recoverPassword(String email) async {
    final http.Response response = await http.post(
      '${WebServiceSettings.apiBase}/auth/recovery',
      headers: WebServiceSettings.getHeaders(false),
      body: jsonEncode(<String, String>{
        'email': email,
      }),
    );
    if (response.statusCode == 201) {
      return "success";
    } else {
      return Future.error("Não foi possível enviar o email, tente novamente.");
    }
  }
}