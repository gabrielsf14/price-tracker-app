import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pricetracker/models/product.dart';
import 'package:pricetracker/webservices/webServiceSettings.dart';

class DeleteProductService {

  Future<void> deleteProduct(Product product) async {
    final http.Response response = await http.delete(
        '${WebServiceSettings.apiBase}/products/${product.id}',
        headers: WebServiceSettings.getHeaders(true)
    );
    if (response.statusCode == 200) {
      return "success";
    } else {
      return Future.error("Não foi possível deletar o produto.");
    }
  }
}