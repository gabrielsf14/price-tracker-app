import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pricetracker/models/product.dart';
import 'package:pricetracker/webservices/webServiceSettings.dart';

class FindProductService {

  Future<Product> findProduct(String id) async {
    final http.Response response = await http.get(
        '${WebServiceSettings.apiBase}/products/$id',
        headers: WebServiceSettings.getHeaders(true)
    );
    if (response.statusCode == 200) {
      return Product.fromJson(json.decode(response.body));
    } else {
      return Future.error("Não foi possível obter o produto.");
    }
  }
}