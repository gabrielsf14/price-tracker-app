import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pricetracker/models/label.dart';
import 'package:pricetracker/models/store.dart';
import 'package:pricetracker/webservices/webServiceSettings.dart';

class GetStoresService {

  Future<List<Store>> getStores() async {
    final http.Response response = await http.get(
        '${WebServiceSettings.lomaddeApiBase}/store/_all?sourceId=36797328'
    );
    if (response.statusCode == 200) {
      Map<String, dynamic> jsonMap = json.decode(response.body);
      Iterable list = jsonMap['stores'];
      var stores = list.map((model) => Store.fromJson(model)).toList();
      return stores;
    } else {
      return Future.error("Não foi possível listar as lojas.");
    }
  }
}