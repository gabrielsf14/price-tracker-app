import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pricetracker/models/category.dart';
import 'package:pricetracker/models/label.dart';
import 'package:pricetracker/models/store.dart';
import 'package:pricetracker/webservices/webServiceSettings.dart';

class GetCategoriesService {

  Future<List<Category>> getCategories() async {
    final http.Response response = await http.get(
        '${WebServiceSettings.lomaddeApiBase}/category/_all?sourceId=36797328'
    );
    if (response.statusCode == 200) {
      Map<String, dynamic> jsonMap = json.decode(response.body);
      Iterable list = jsonMap['categories'];
      var categories = list.map((model) => Category.fromJson(model)).toList();
      return categories;
    } else {
      return Future.error("Não foi possível listar as categorias.");
    }
  }
}