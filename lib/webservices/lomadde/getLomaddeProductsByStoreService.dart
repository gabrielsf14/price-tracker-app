import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pricetracker/models/label.dart';
import 'package:pricetracker/models/lomaddeProduct.dart';
import 'package:pricetracker/models/store.dart';
import 'package:pricetracker/webservices/webServiceSettings.dart';

class GetLomaddeProductsByStoreService {

  Future<List<LomaddeProduct>> getProducts(int currentPage, Store store) async {
    final http.Response response = await http.get(
        '${WebServiceSettings.lomaddeApiBase}/offer/_store/${store.id}?sourceId=36797328&page=$currentPage'
    );
    if (response.statusCode == 200) {
      Map<String, dynamic> jsonMap = json.decode(response.body);
      Iterable list = jsonMap['offers'];
      var products = list.map((model) => LomaddeProduct.fromJson(model)).toList();
      return products;
    } else {
      return Future.error("Não foi possível listar os produtos.");
    }
  }
}