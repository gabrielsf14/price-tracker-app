import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:pricetracker/webservices/webServiceSettings.dart';

class AddFcmTokenService {

  Future<void> addToken(String token) async {
    final http.Response response = await http.post(
      '${WebServiceSettings.apiBase}/users/register-token',
      headers: WebServiceSettings.getHeaders(true),
      body: jsonEncode(<String, String>{
        'value': token,
        'plataform': Platform.isIOS ? 'ios' : 'android'
      }),
    );
    if (response.statusCode == 201) {
      return "success";
    } else {
      return Future.error("Não foi possível cadastrar o token.");
    }
  }
}