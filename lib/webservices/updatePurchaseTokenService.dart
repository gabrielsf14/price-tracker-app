import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pricetracker/webservices/webServiceSettings.dart';

class UpdatePurchaseTokenService {
  Future<void> updatePurchaseToken(String purchaseToken) async {
    final http.Response response = await http.patch(
      '${WebServiceSettings.apiBase}/users/purchase-token',
      headers: WebServiceSettings.getHeaders(true),
      body: jsonEncode(<String, String>{
        "purchaseToken": purchaseToken
      }),
    );
    if (response.statusCode == 200) {
      return "success";
    } else {
      return Future.error("Não foi possível atualizar o usuário, verifique sua conexão e tente novamente.");
    }
  }
}