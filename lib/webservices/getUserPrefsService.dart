import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pricetracker/models/userPrefs.dart';
import 'package:pricetracker/webservices/webServiceSettings.dart';

class GetUserPrefsService {

  Future<UserPrefs> getPrefs() async {
    final http.Response response = await http.get(
        '${WebServiceSettings.apiBase}/users/nofification-preference',
        headers: WebServiceSettings.getHeaders(true)
    );
    if (response.statusCode == 200) {
      return UserPrefs.fromJson(json.decode(response.body));
    } else {
      return Future.error("Não foi possível obter as preferências.");
    }
  }
}