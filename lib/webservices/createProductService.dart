import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pricetracker/models/product.dart';
import 'package:pricetracker/webservices/webServiceSettings.dart';

class CreateProductService {
  Future<Product> createProduct(String url) async {
    final http.Response response = await http.post(
      '${WebServiceSettings.apiBase}/products',
      headers: WebServiceSettings.getHeaders(true),
      body: jsonEncode(<String, String>{
        'url': url
      }),
    );
    if (response.statusCode == 201) {
      var body = response.body;
      var productMap = json.decode(body);
      return Product.fromJson(productMap);
    } else {
      return Future.error("Não foi possível cadastrar o produto, verifique sua conexão e tente novamente.");
    }
  }
}