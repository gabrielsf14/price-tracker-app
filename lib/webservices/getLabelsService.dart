import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pricetracker/models/label.dart';
import 'package:pricetracker/webservices/webServiceSettings.dart';

class GetLabelsService {

  Future<List<Label>> getLabels(int page) async {
    final http.Response response = await http.get(
        '${WebServiceSettings.apiBase}/labels?limit=10&page=$page',
        headers: WebServiceSettings.getHeaders(true)
    );
    if (response.statusCode == 200) {
      Map<String, dynamic> jsonMap = json.decode(response.body);
      Iterable list = jsonMap['items'];
      var labels = list.map((model) => Label.fromJson(model)).toList();
      return labels;
    } else {
      return Future.error("Não foi possível obter as coleções.");
    }
  }
}