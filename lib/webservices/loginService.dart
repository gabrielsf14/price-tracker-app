import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pricetracker/models/user.dart';
import 'package:pricetracker/repositories/UserRepository.dart';
import 'package:pricetracker/webservices/webServiceSettings.dart';

class LoginService {

  Future<User> login(String email, String password) async {
    final http.Response response = await http.post(
      '${WebServiceSettings.apiBase}/auth/signin',
      headers: WebServiceSettings.getHeaders(false),
      body: jsonEncode(<String, String>{
        'email': email,
        'password': password,
      }),
    );
    if (response.statusCode == 201) {
      var user = User.fromJson(json.decode(response.body));
      user.password = password;
      UserRepository.instance.saveUser(user);
      return user;
    } else if (response.statusCode == 401) {
      return Future.error("Email ou senha incorretos, verifique seus dados e tente novamente.");
    } else {
      return Future.error("Não foi possível entrar na conta, verifique sua conexão e tente novamente.");
    }
  }
}