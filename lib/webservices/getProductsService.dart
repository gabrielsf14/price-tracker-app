import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pricetracker/models/product.dart';
import 'package:pricetracker/webservices/webServiceSettings.dart';

class GetProductsService {

  Future<List<Product>> getProducts(int currentPage, String labelId) async {
    final http.Response response = await http.get(
        '${WebServiceSettings.apiBase}/products?limit=10&page=$currentPage${labelId.isNotEmpty ? "&label=$labelId" : ""}',
        headers: WebServiceSettings.getHeaders(true)
    );
    if (response.statusCode == 200) {
      Map<String, dynamic> jsonMap = json.decode(response.body);
      Iterable list = jsonMap['items'];
      var products = list.map((model) => Product.fromJson(model)).toList();
      return products;
    } else {
      return Future.error("Não foi possível obter os produtos.");
    }
  }
}