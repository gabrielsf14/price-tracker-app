import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pricetracker/models/userPrefs.dart';
import 'package:pricetracker/webservices/webServiceSettings.dart';

class UpdateUserPrefsService {

  Future<void> updatePrefs(UserPrefs prefs) async {
    final http.Response response = await http.patch(
        '${WebServiceSettings.apiBase}/users/nofification-preference',
        headers: WebServiceSettings.getHeaders(true),
        body: jsonEncode(<String, dynamic>{
          'enabled': prefs.enabled,
          'rate': double.parse(prefs.rate),
        }),
    );
    if (response.statusCode == 200) {
      return "success";
    } else {
      return Future.error("Não foi possível atualizar as preferências.");
    }
  }
}