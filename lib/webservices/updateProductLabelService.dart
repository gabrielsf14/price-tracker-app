import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pricetracker/models/product.dart';
import 'package:pricetracker/webservices/webServiceSettings.dart';

class UpdateProductLabelService {
  Future<Product> updateProductLabel(String productId, String name, String color) async {
    final http.Response response = await http.patch(
      '${WebServiceSettings.apiBase}/products/$productId',
      headers: WebServiceSettings.getHeaders(true),
      body: jsonEncode(<String, String>{
        "name": name,
        "color": color
      }),
    );
    if (response.statusCode == 200) {
      var body = response.body;
      var productMap = json.decode(body);
      return Product.fromJson(productMap);
    } else {
      return Future.error("Não foi possível atualizar o produto, verifique sua conexão e tente novamente.");
    }
  }
}