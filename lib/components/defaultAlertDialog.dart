import 'package:flutter/material.dart';
import 'package:pricetracker/design_configs/appColors.dart';

class DefaultAlertDialog {

  final String title;
  final String message;
  Function onClosed = (){};

  DefaultAlertDialog(this.title, this.message);

  Future<void> show(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            title,
            style: TextStyle (
                color: AppColors.appSecondaryColor
            ),
          ),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text(
                "FECHAR",
                style: TextStyle (
                  color: AppColors.appSecondaryColor
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
                onClosed();
              },
            ),
          ],
        );
      },
    );
  }
}