
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:pricetracker/design_configs/appColors.dart';

class CreateLabelDialog {

  Color pickerColor = Color(0xff443a49);
  TextEditingController textFieldController = TextEditingController();

  void changeColor(Color color) {
    pickerColor = color;
  }

  void show(BuildContext context, Function(String, Color) onSavePressed) {
// raise the [showDialog] widget
    showDialog(
      context: context,
      child: AlertDialog(
        title: const Text('Escolha uma cor'),
        content: SingleChildScrollView(
           child: BlockPicker(
             pickerColor: pickerColor,
             onColorChanged: changeColor,
           ),
        ),
        actions: <Widget>[
          FlatButton(
            child: new Text(
              'CANCELAR',
              style: TextStyle (
                  color: AppColors.appSecondaryColor
              ),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          FlatButton(
            child: const Text("SELECIONAR",
              style: TextStyle (
                color: AppColors.appSecondaryColor
              ),
            ),
            onPressed: () {
              Navigator.of(context).pop();
              showDialog (
                context: context,
                  child: AlertDialog(
                    title: const Text('Digite o nome'),
                    content: Container (
                      padding: EdgeInsets.only(left: 10, right: 10),
                      color: AppColors.appTextFieldBGColor,
                      child: TextField(
                        autofocus: true,
                        controller: textFieldController,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "Nome da coleção",
                          hintStyle: TextStyle (
                              color: AppColors.appTextFieldHintColor
                          ),
                        ),
                        style: TextStyle (
                          color: AppColors.appSecondaryColor,
                        ),
                      ),
                    ),
                    actions: <Widget>[
                      FlatButton(
                        child: new Text(
                          'CANCELAR',
                          style: TextStyle (
                              color: AppColors.appSecondaryColor
                          ),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      FlatButton(
                        child: new Text(
                          'SALVAR',
                          style: TextStyle (
                              color: AppColors.appSecondaryColor
                          ),
                        ),
                        onPressed: () => {
                          if (textFieldController.text != null) {
                            onSavePressed(textFieldController.text, pickerColor)
                          }
                        },
                      )
                    ],
                )
              );
            },
          ),
        ],
      ),
    );
  }
}