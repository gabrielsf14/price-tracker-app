import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pricetracker/design_configs/appColors.dart';

import 'clickableText.dart';

class AddProductAlertDialog  {

  final TextEditingController textFieldController = TextEditingController();

  void show(BuildContext context, Function(String) onSavePressed) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(
              'Adicionar produto',
              style: TextStyle (
                  color: AppColors.appSecondaryColor
              ),
            ),
            content: Column (
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container (
                  padding: EdgeInsets.only(left: 10, right: 10),
                  color: AppColors.appTextFieldBGColor,
                  child: TextField(
                    controller: textFieldController,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "Insira ou cole o link do produto",
                      hintStyle: TextStyle (
                          color: AppColors.appTextFieldHintColor
                      ),
                    ),
                    style: TextStyle (
                      color: AppColors.appSecondaryColor,
                    ),
                  ),
                ),
                Padding (
                  padding: EdgeInsets.only(top: 20),
                  child: Align (
                    alignment: Alignment.topRight,
                    child: ClickableText("Colar link", () => {
                      Clipboard.getData('text/plain').then((clipboardContent) {
                        textFieldController.text = clipboardContent.text;
                      })
                    }),
                  )
                )
              ],
            ),
            actions: <Widget>[
              FlatButton(
                child: new Text(
                  'CANCELAR',
                  style: TextStyle (
                      color: AppColors.appSecondaryColor
                  ),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                child: new Text(
                  'ADICIONAR',
                  style: TextStyle (
                      color: AppColors.appSecondaryColor
                  ),
                ),
                onPressed: () => {
                  if (textFieldController.text != null) {
                    onSavePressed(textFieldController.text)
                  }
                },
              )
            ],
          );
        });
  }
}