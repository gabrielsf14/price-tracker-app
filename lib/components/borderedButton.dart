import 'package:flutter/material.dart';
import 'package:pricetracker/design_configs/appColors.dart';
import 'package:pricetracker/design_configs/appDesignValues.dart';

class BorderedButton extends StatelessWidget {

  final String title;
  final Function onPressed;

  const BorderedButton(this.title, this.onPressed);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: AppDesignValues.normalTopPadding),
      child: FlatButton(
          padding: EdgeInsets.all(10),
          color: AppColors.appSecondaryColor,
          textColor: Colors.white,
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(5)),
          child: Text(title),
          onPressed: onPressed),
    );
  }
}
