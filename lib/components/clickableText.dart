import 'package:flutter/material.dart';
import 'package:pricetracker/design_configs/appColors.dart';
import 'package:pricetracker/design_configs/appDesignValues.dart';

class ClickableText extends StatelessWidget {

  final String text;
  final Function onPressed;

  ClickableText(this.text, this.onPressed);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: new Text(
        text,
        style: TextStyle (
            fontSize: AppDesignValues.actionTextFontSize,
            fontWeight: FontWeight.bold,
            color: AppColors.appSecondaryColor
        ),
      ),
    );
  }
}