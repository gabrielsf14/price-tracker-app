import 'package:flutter/material.dart';
import 'package:pricetracker/design_configs/appColors.dart';
import 'package:pricetracker/design_configs/appDesignValues.dart';

class BorderedTextFieldWithIcon extends StatelessWidget {

  final String hint;
  final IconData icon;
  final TextInputAction action;
  final Function(String) onSubmitted;
  final TextEditingController controller;
  final TextInputType type;
  final bool obscureText;

  const BorderedTextFieldWithIcon(this.hint, this.icon, this.action, this.onSubmitted, this.controller, this.type, this.obscureText);

  @override
  Widget build(BuildContext context) {
    return Container (
        height: 50,
        padding: EdgeInsets.only(left: AppDesignValues.defaultPadding, right: AppDesignValues.defaultPadding),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(5)),
        ),
        child: Center (
            child: Row (
              children: <Widget>[
                Expanded (
                  child: TextField(
                    obscureText: obscureText,
                    keyboardType: type,
                    controller: controller,
                    onSubmitted: onSubmitted,
                    textInputAction: action,
                    style: TextStyle (
                        color: AppColors.appSecondaryColor
                    ),
                    decoration: InputDecoration(
                        hintText: hint,
                        hintStyle: TextStyle (
                            color: AppColors.appTextFieldHintColor
                        ),
                        border: InputBorder.none
                    ),
                  ),
                ),
                Icon(icon, color: AppColors.appSecondaryColor)
              ],
            )
        )
    );
  }
}
