import 'package:flutter/cupertino.dart';

class CircularNetworkImage extends StatelessWidget{

  final double width;
  final double height;
  final String url;

  CircularNetworkImage(this.width, this.height, this.url);

  @override
  Widget build(BuildContext context) {
    return new Container(
        width: width,
        height: height,
        decoration: new BoxDecoration(
            shape: BoxShape.circle,
            image: new DecorationImage(
                fit: BoxFit.fill,
                image: url != null && url.isNotEmpty ? new NetworkImage(url) : new AssetImage("assets/placeholder.png")
            )
        ));
  }
}