import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RectangularNetworkImage extends StatelessWidget{

  final double width;
  final double height;
  final String url;

  RectangularNetworkImage(this.width, this.height, this.url);

  @override
  Widget build(BuildContext context) {
    return new Container(
        width: width,
        height: height,
        decoration: new BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            shape: BoxShape.rectangle,
            image: new DecorationImage(
                fit: BoxFit.contain,
                image: url != null && url.isNotEmpty ? new NetworkImage(url) : new AssetImage("assets/placeholder.png")
            )
        ));
  }
}