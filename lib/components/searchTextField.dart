import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SearchTextField extends StatelessWidget {

  Function(String) onSearchTextChanged;
  Function(String) onSubmitted;

  SearchTextField(this.onSearchTextChanged, {this.onSubmitted});

  @override
  Widget build(BuildContext context) {
    return Container (
        padding: EdgeInsets.only(top: 18, left: 12),
        color: Colors.black12,
        height: 38,
        child: TextField(
          autofocus: true,
          textInputAction: TextInputAction.search,
          style: new TextStyle(
            color: Colors.black,
          ),
          decoration: InputDecoration(
              hintText: "Pesquisar",
              hintStyle: TextStyle(color: Colors.black45),
              border: InputBorder.none
          ),
          onChanged: (text) {
            onSearchTextChanged(text);
          },
          onSubmitted: (text) {
            onSubmitted(text);
          },
        )
    );
  }
}