import 'package:flutter/material.dart';
import 'package:pricetracker/app_configs/appConfigs.dart';
import 'package:pricetracker/design_configs/appColors.dart';
import 'package:pricetracker/design_configs/appDesignValues.dart';

class DefaultAppBar {

  static AppBar build() {
    return AppBar(
      iconTheme: IconThemeData(
          color: AppColors.appSecondaryColor
      ),
      title: const Text(AppConfigs.appName,
          style: TextStyle(color: AppColors.appSecondaryColor, fontSize: AppDesignValues.appBarTitleFontSize)),
    );
  }
}