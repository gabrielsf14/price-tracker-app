import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:pricetracker/constants/routePaths.dart';
import 'package:pricetracker/pages/bottomNavigationPage.dart';
import 'package:pricetracker/pages/productDetailPage.dart';
import 'package:pricetracker/services/navigationService.dart';
import 'package:pricetracker/webservices/addFcmTokenService.dart';
import 'package:pricetracker/webservices/findProductService.dart';

import 'locator.dart';

Future<dynamic> _onBackgroundMessage(Map<String, dynamic> message) async {
  print('On background message $message');
  return Future<void>.value();
}

class PushNotificationsManager {

  PushNotificationsManager._();

  factory PushNotificationsManager() => _instance;

  static final PushNotificationsManager _instance = PushNotificationsManager._();

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  bool _initialized = false;

  final NavigationService navigationService = locator<NavigationService>();

  Future<void> init() async {
    if (!_initialized) {
      // For iOS request permission first.
      _firebaseMessaging.requestNotificationPermissions();
      _firebaseMessaging.configure();

      // For testing purposes print the Firebase Messaging token
      String token = await _firebaseMessaging.getToken();
      print("FirebaseMessaging token: $token");
      AddFcmTokenService().addToken(token);

      _firebaseMessaging.configure(
          onMessage: (msg) {
            print(msg);
            return;
          },
          onLaunch: (msg) {
            loadProduct(msg['data']['productId']);
            return;
          },
          onResume: (msg) {
            loadProduct(msg['data']['productId']);
            return;
          },
          onBackgroundMessage: _onBackgroundMessage
      );

      _initialized = true;
    }
  }

  void loadProduct(String id) {
    FindProductService().findProduct(id).then((product) {
      navigationService.navigateTo(ProductDetailRoute, product);
    });
  }
}