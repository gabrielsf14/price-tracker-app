class UserPrefs {

  bool enabled;
  String rate;

  UserPrefs(this.enabled, this.rate);

  UserPrefs.fromJson(Map<String, dynamic> json) :
        enabled = json['enabled'],
        rate = json['rate'];

  Map<String, dynamic> toJson() => {
    'enabled': enabled,
    'rate': rate,
  };
}