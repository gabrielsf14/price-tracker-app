class Site {
  String id;
  String name;
  String image;
  String domain;

  Site(this.id, this.name, this.image, this.domain);

  Site.fromJson(Map<String, dynamic> json) :
        id = json['id'],
        name = json['name'],
        image = json['image'],
        domain = json['domain'];

  Map<String, dynamic> toJson() => {
    'name': name,
    'id': id,
    'image': image,
    'domain': domain
  };

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is Site &&
              runtimeType == other.runtimeType &&
              id == other.id;

  @override
  int get hashCode => id.hashCode;
}