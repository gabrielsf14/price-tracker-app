import 'package:pricetracker/models/site.dart';

import 'label.dart';

class Product {

  String id;
  String title;
  String description;
  String image;
  String currentPrice;
  String originalPrice;
  String currency;
  String url;
  String createdAt;
  String updatedAt;
  Site site;
  Label label;

  Product(this.id, this.title, this.description, this.image, this.currentPrice, this.originalPrice, this.currency, this.url, this.site, this.label, this.createdAt , this.updatedAt);

  Product.fromJson(Map<String, dynamic> json) :
        id = json['id'],
        title = json['title'],
        description = json['description'],
        image = json['image'],
        currentPrice = json['currentPrice'],
        originalPrice = json['originalPrice'],
        currency = json['currency'],
        url = json['url'],
        createdAt = json['createdAt'],
        updatedAt = json['updatedAt'],
        site =  json['site'] != null ? Site.fromJson(json['site']) : null,
        label = json['label'] != null ? Label.fromJson(json['label']) : null;

  Map<String, dynamic> toJson() => {
    'id': id,
    'title': title,
    'description': description,
    'image': image,
    'currentPrice': currentPrice,
    'originalPrice': originalPrice,
    'currency': currency,
    'url': url,
    'createdAt': createdAt,
    'updatedAt': updatedAt,
    'site': site != null ? site.toJson() : null,
    'label' : label != null ? label.toJson() : null
  };

  double getCurrentPriceAsDouble() {
    return double.parse(currentPrice);
  }

  String formattedPrice() {
    return originalPrice.replaceAll(".", ",");
  }

  String formattedCurrentPrice() {
    return currentPrice.replaceAll(".", ",");
  }

  DateTime getUpdatedAtAsDate() {
    return DateTime.parse(updatedAt);
  }

  DateTime getCreatedAtAsDate() {
    return DateTime.parse(createdAt);
  }

  double getPriceChange() {
    var originalPriceDouble = double.parse(originalPrice);
    var diff = double.parse(currentPrice) - originalPriceDouble;
    return (diff / originalPriceDouble) * 100;
  }

  String getPriceChangeFormatted() {
    var priceChange = getPriceChange();
    var priceChangeString = priceChange.toStringAsFixed(2).replaceAll(".", ",") + "%";
    if (priceChange > 0) {
      priceChangeString = "+" + priceChangeString;
    }
    return priceChangeString;
  }

  bool priceChangeIsPositive() {
    return getPriceChange() > 0;
  }

  bool priceChangeIsZero() {
    return getPriceChange() == 0;
  }
}