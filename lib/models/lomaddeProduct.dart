import 'package:pricetracker/models/category.dart';
import 'package:pricetracker/models/store.dart';

class LomaddeProduct {

  String id;
  String name;
  double price;
  double priceFrom;
  double discount;
  Category category;
  Store store;
  String link;
  String thumbnail;

  LomaddeProduct(this.id, this.name, this.price, this.priceFrom, this.discount,
      this.category, this.store, this.link, this.thumbnail);

  LomaddeProduct.fromJson(Map<String, dynamic> json) :
        id = json['id'],
        name = json['name'],
        price = json['price'],
        priceFrom = json['priceFrom'],
        discount = json['discount'],
        link = json['link'],
        thumbnail = json['thumbnail'],
        category =  json['category'] != null ? Category.fromJson(json['category']) : null,
        store = json['store'] != null ? Store.fromJson(json['store']) : null;

  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
    'price': price,
    'priceFrom': priceFrom,
    'discount': discount,
    'link': link,
    'thumbnail': thumbnail,
    'category': category != null ? category.toJson() : null,
    'store' : store != null ? store.toJson() : null
  };

  String formattedPrice() {
    return price.toStringAsFixed(2).replaceAll(".", ",");
  }

  String formattedPriceFrom() {
    return priceFrom.toStringAsFixed(2).replaceAll(".", ",");
  }
}