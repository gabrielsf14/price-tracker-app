class Store {

  int id;
  String name;
  String thumb;

  Store(this.id, this.name, this.thumb);

  Store.fromJson(Map<String, dynamic> json) :
        thumb = json['thumbnail'],
        id = json['id'],
        name = json['name'];

  Map<String, dynamic> toJson() => {
    'id': id,
    'thumbnail': thumb,
    'name' : name
  };

}