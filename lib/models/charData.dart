class ChartData {

  DateTime time;
  double value;

  ChartData(this.time, this.value);
}