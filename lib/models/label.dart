import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:pricetracker/utils/hexColor.dart';

class Label {

  String id;
  String name;
  String color;

  Label(this.id, this.name, this.color);

  Label.fromJson(Map<String, dynamic> json) :
        color = json['color'],
        id = json['id'],
        name = json['name'];

  Map<String, dynamic> toJson() => {
    'id': id,
    'color': color,
    'name' : name
  };

  Color getColorFromHex() {
    return HexColor.fromHex(color);
  }
}