class PriceChange {
  final String updatedAt;
  final String previousPrice;
  final String currentPrice;

  PriceChange(this.updatedAt, this.previousPrice, this.currentPrice);

  PriceChange.fromJson(Map<String, dynamic> json) :
        updatedAt = json['updatedAt'],
        previousPrice = json['previousPrice'],
        currentPrice = json['currentPrice'];

  Map<String, dynamic> toJson() => {
    'updatedAt': updatedAt,
    'previousPrice': previousPrice,
    'currentPrice': currentPrice
  };

  DateTime getUpdatedAtAsDate() {
    return DateTime.parse(updatedAt.split('T').first);
  }

  String formattedPreviousPrice() {
    return previousPrice.replaceAll(".", ",");
  }

  String formattedCurrentPrice() {
    return currentPrice.replaceAll(".", ",");
  }
}