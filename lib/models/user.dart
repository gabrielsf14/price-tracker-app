class User {

  String name;
  String email;
  String token;
  String password;
  String status;

  User();

  User.fromJson(Map<String, dynamic> json) :
        name = json['name'],
        email = json['email'],
        password = json['password'],
        status = json['status'],
        token = json['jwtToken'];

  Map<String, dynamic> toJson() => {
    'name': name,
    'email': email,
    'jwtToken': token,
    'password': password,
    'status': status,
  };

//  (1) SUBSCRIPTION_RECOVERED: uma assinatura foi recuperada da suspensão de conta.
//  (2) SUBSCRIPTION_RENEWED: uma assinatura ativa foi renovada.
//  (3) SUBSCRIPTION_CANCELED: uma assinatura foi cancelada de forma voluntária ou involuntária. Para cancelamento voluntário, é enviada quando o usuário faz o cancelamento.
//  (4) SUBSCRIPTION_PURCHASED: uma nova assinatura foi comprada.
//  (5) SUBSCRIPTION_ON_HOLD: uma assinatura entrou na suspensão de conta (se ativada).
//  (6) SUBSCRIPTION_IN_GRACE_PERIOD: uma assinatura entrou no período de carência (se ativado).
//  (7) SUBSCRIPTION_RESTARTED: o usuário reativou a assinatura em Play > Conta > Assinaturas (requer ativação da restauração de assinatura).
//  (8) SUBSCRIPTION_PRICE_CHANGE_CONFIRMED: uma alteração no preço da assinatura foi confirmada pelo usuário.
//  (9) SUBSCRIPTION_DEFERRED: o tempo de renovação de uma assinatura foi estendido.
//  (10) SUBSCRIPTION_PAUSED: uma assinatura foi pausada.
//  (11) SUBSCRIPTION_PAUSE_SCHEDULE_CHANGED: a programação de uma pausa na assinatura foi alterada.
//  (12) SUBSCRIPTION_REVOKED: uma assinatura foi revogada pelo usuário antes do prazo de vencimento.
//  (13) SUBSCRIPTION_EXPIRED: a assinatura expirou.
  bool isPro() {
    return status != null && status != "SUBSCRIPTION_CANCELED" && status != "SUBSCRIPTION_EXPIRED" &&
        status != "SUBSCRIPTION_REVOKED" && status != "SUBSCRIPTION_ON_HOLD";
  }
}