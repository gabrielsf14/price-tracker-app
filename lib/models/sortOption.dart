enum SortOption {
  date,
  alphabetic,
  price
}

extension SortOptionExtension on SortOption {

  String getName() {
    switch (this) {
      case SortOption.price:
        return 'Melhores preços';
      case SortOption.date:
        return 'Data de adição';
      case SortOption.alphabetic:
        return 'Ordem alfabética';
      default:
        return null;
    }
  }
}