abstract class Repository {

  void save();
  void load();
  void clear();
}