import 'package:pricetracker/models/user.dart';
import 'package:pricetracker/repositories/Repository.dart';
import 'package:pricetracker/storage/SharedPrefsStorage.dart';

class UserRepository implements Repository {

  UserRepository._();

  static final UserRepository _instance = UserRepository._();

  static UserRepository get instance { return _instance;}

  final String key = "user";
  User user;

  @override
  void clear() {
    user = null;
    SharedPrefsStorage.remove(key);
  }

  @override
  void load() {
    var userJson = SharedPrefsStorage.read(key);
    user = userJson == null ? null : User.fromJson(userJson);
  }

  @override
  void save() {
    SharedPrefsStorage.save(key, user.toJson());
  }

  bool userExists() {
    return user != null;
  }

  void saveUser(User user) {
    this.user = user;
    save();
  }
}